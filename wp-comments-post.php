<?php
/**
 * Handles Comment Post to WordPress and prevents duplicate comment posting.
 *
 * @package WordPress
 */

if ( 'POST' != $_SERVER['REQUEST_METHOD'] ) {
	$protocol = $_SERVER['SERVER_PROTOCOL'];
	if ( ! in_array( $protocol, array( 'HTTP/1.1', 'HTTP/2', 'HTTP/2.0' ) ) ) {
		$protocol = 'HTTP/1.0';
	}

	header('Allow: POST');
	header("$protocol 405 Method Not Allowed");
	header('Content-Type: text/plain');
	exit;
}

/** Sets up the WordPress Environment. */
require( dirname(__FILE__) . '/wp-load.php' );

nocache_headers();

$comment = wp_handle_comment_submission( wp_unslash( $_POST ) );

if ( is_wp_error( $comment ) ) {
	$data = intval( $comment->get_error_data() );
	if ( ! empty( $data ) ) {
		wp_die( '<p>' . $comment->get_error_message() . '</p>', __( 'Comment Submission Failure' ), array( 'response' => $data, 'back_link' => true ) );
	} else {
		exit;
	}
}

$user = wp_get_current_user();

/**
 * Perform other actions when comment cookies are set.
 *
 * @since 3.4.0
 *
 * @param WP_Comment $comment Comment object.
 * @param WP_User    $user    User object. The user may not exist.
 */
do_action( 'set_comment_cookies', $comment, $user );

$location = empty( $_POST['redirect_to'] ) ? get_comment_link( $comment ) : $_POST['redirect_to'] . '#comment-' . $comment->comment_ID;

// send review data to contact@lernable.com
$post_id = $_POST['comment_post_ID'];

$internal_message =
	'<!DOCTYPE html>' .
	'<html>' .
		'<head>' .
			'<title>New Review</title>' .
			'<link rel="stylesheet" href="https://lernable.com/wp-content/themes/lernable/style.css">' .
		'</head>' .
		'<body>' .
			'<div class="internal-review-data">' .
				'<p class="internal-review-text">Reviewer: ' . $_POST['author'] . ' (' . $_POST['email'] . ')' . '</p>' .
				'<p class="internal-review-text">Lesson: ' . get_the_title($post_id) . ' (ID: ' . $post_id . ')' . '</p>' .
				'<p class="internal-review-text">Public Review:</p>' .
				'<p class="public-review">' . ( isset($_POST['comment']) ? $_POST['comment'] : 'None.' ) . '</p>' .
				'<p class="private-message">Improvement Areas: ';
	
$num_areas = count($_POST['improve']);

if($num_areas == 0) $internal_message .= 'None chosen.';
for($i = 0; $i < $num_areas; $i++) {
	$internal_message .= $_POST['improve'][$i];
	if($i+1 != $num_areas) {
		$internal_message .= ', ';
	}
}

$internal_message .= '</p>';

$internal_message .=
				'<p class="internal-review-text">Host Improvement:</p>' .
				'<p class="private-message">' . (isset($_POST['improvement-text']) ? $_POST['improvement-text'] : 'None.' ). '</p>' .
				'<p class="internal-review-text">Just for Lernable:</p>' .
				'<p class="just-for-lernable">' . (isset($_POST['lernable-feedback-text']) ? $_POST['lernable-feedback-text'] : 'None.') . '</p>' .
			'</div>' .
		'</body>' .
	'</html>';

wp_mail( 'contact@lernable.com', 'Review from ' . $_POST['author'] . ' (ID: ' . $comment->comment_ID . ')', $internal_message);

// send host message to host
$product = wc_get_product($post_id);
$host_email = $product->get_attribute( 'host_email' );

if(isset($_POST['improve']) || isset($_POST['improvement-text'])) {

	$host_message =
		'<!DOCTYPE html>' .
		'<html>' .
			'<head>' .
				'<title>Message for you</title>' .
				'<link rel="stylesheet" href="https://lernable.com/wp-content/themes/lernable/style.css">' .
			'</head>' .
			'<body>' .
				'<div class="host-message-data">' .
					'<p class="host-message-text">Reviewer: ' . $_POST['author'] . ' (' . $_POST['email'] . ')' . '</p>' .
					'<p class="host-message-text">Lesson: ' . get_the_title($post_id) . '</p>' .
					'<p class="host-message-text">The areas I think the experience may be improved: ';

	if($num_areas == 0) $host_message .= 'None chosen.';
	for($i = 0; $i < $num_areas; $i++) {
		$host_message .= $_POST['improve'][$i];
		if($i+1 != $num_areas) {
			$host_message .= ', ';
		}
	}

	$host_message .= '</p>';
	if(isset($_POST['improvement-text'])) {
		$host_message .= '<p class="host-message-text">This is the message for you:</p>';
		$host_message .= '<p class="message">' . $_POST['improvement-text'] . '</p>';
	}
	
	$host_message .=
				'</div>' .
			'</body>' .
		'</html>';

	// wp_mail( $host_email , 'Lernable - Message to you from ' . $_POST['author'] , $host_message, 'From: Lernable <contact@lernable.com>');
}
/**
 * Filters the location URI to send the commenter after posting.
 *
 * @since 2.0.5
 *
 * @param string     $location The 'redirect_to' URI sent via $_POST.
 * @param WP_Comment $comment  Comment object.
 */
$location = apply_filters( 'comment_post_redirect', $location, $comment );

wp_safe_redirect( $location );
exit;
