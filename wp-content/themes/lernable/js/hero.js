// this file contains functions which control the hero-image carousel

var slideIndex = 1;
showDivs(slideIndex);

function nextDiv() {
    showDivs(slideIndex += 1);
}

function prevDiv() {
    showDivs(slideIndex -= 1);
}

function currentDiv(n) {
    showDivs(slideIndex = n);
}

function showDivs(n) {
    var i;
    var x = document.getElementsByClassName("hero-image");
    console.log(x);
    var dots = document.getElementsByClassName("carousel-badge");
    if (n > x.length) {slideIndex = 1} 
    if (n < 1) {slideIndex = x.length} ;
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none"; 
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].style.opacity = "0.4";
    }
    x[slideIndex-1].style.display = "block";
    dots[slideIndex-1].style.opacity = "1";
    
    var tagl = document.getElementById("tagline");
    var desc = document.getElementById("description");
    
    if(slideIndex != 1) {
        tagl.style.opacity = "0";
        desc.style.opacity = "0";
    } else {
        tagl.style.opacity = "1";
        desc.style.opacity = "1";       
    }
}