// the functions defined in this file control the functionality of the booking calendar

clearLessonBlocks();
displayDaysWithLessons();
displayToday();
firstRemoveLeftArrow();

function clearLessonBlocks() {
	var count_days = 1;
	var now = new Date();
	var daysInMonth = new Date(now.getFullYear(), now.getMonth() + 1, 0).getDate();
	var count_months = 0;
	while (count_months < 4) {
		count_days = 1;
		while (count_days <= daysInMonth) {
			var lesson_block = document.getElementById("lesson-block-" + count_months + "-" + count_days);
			if(lesson_block != null) {
				lesson_block.style.display = "none";
				var current = document.getElementById("li-" + count_months + "-" + count_days);
				current.style.backgroundColor = "transparent";
				current.style.color = "black";
			}
			count_days += 1;
		}
		count_months += 1;
	}
}

function displayToday() {
	var now = new Date();
	var day_num = now.getDate();
	var today_label = document.getElementById("li-0-" + day_num)
	//today_label.style.backgroundColor = "#5DC6BB";
	today_label.style.backgroundColor = "#404040";
	today_label.style.color = "white";
	today_label.style.borderRadius = "100%";
}

function showLessonBlock(day, month) {
	clearLessonBlocks();
	displayToday();
	var span = document.getElementById("has_lessons-" + month + "-" + day);
	if (span != null) {
		var lesson_block = document.getElementById("lesson-block-" + month + "-" + day);
		lesson_block.style.display = "block";
		var current = document.getElementById("li-" + month + "-" + day);
		current.style.backgroundColor = "#5DC6BB";
		current.style.color = "white";
		current.style.borderRadius = "100%";
	}
}

function displayDaysWithLessons() {
	var count_days = 1;
	var count_months = 0;

	while (count_months < 4) {
		count_days = 1;
		while (count_days <= 31) {
			var span = document.getElementById("has_lessons-" + count_months + "-" + count_days);
			if (span != null) {
				var dot = document.getElementById("available_lessons-" + count_months + "-" + count_days);
				dot.style.display = "block";
				var day = document.getElementById("li-" + count_months + "-" + count_days);
				day.style.cursor = "pointer";
			}
			count_days += 1;
		}
		count_months += 1;
	}
}

function nextCal() {
	clearLessonBlocks();
	displayToday();
	var count_months = 0;
	var indicator = 0;
	while (count_months < 3 && indicator == 0) {
		var calendar = document.getElementById("calendar-" + count_months);
		if (calendar.style.display != "none") {
			calendar.style.display = "none";
			indicator = 1;
		}
		count_months += 1;
	}
	if (count_months < 4) {
		calendar = document.getElementById("calendar-" + count_months);
		calendar.style.display = "block";
		if (count_months == 3) {
			var next_arrow = document.getElementById("next-" + count_months);
			next_arrow.style.opacity = "0";
			next_arrow.style.cursor = "default";
		}
		var prev_arrow = document.getElementById("prev-" + count_months);
		prev_arrow.style.opacity = "1";
		prev_arrow.style.cursor = "pointer";
	}
}

function prevCal() {
	clearLessonBlocks();
	displayToday();
	var count_months = 1;
	var indicator = 0;
	while (count_months < 4 && indicator == 0) {
		var calendar = document.getElementById("calendar-" + count_months);
		if (calendar.style.display != "none") {
			calendar.style.display = "none";
			indicator = 1;
		}
		count_months += 1;
	}
	if (count_months != 4 || indicator == 1) {
		count_months -= 2;
		if (count_months >= 0) {
			calendar = document.getElementById("calendar-" + count_months);
			calendar.style.display = "block";
			if (count_months == 0) {
				var prev_arrow = document.getElementById("prev-" + count_months);
				prev_arrow.style.opacity = "0";
				prev_arrow.style.cursor = "default";
			}
			var next_arrow = document.getElementById("next-" + count_months);
			next_arrow.style.opacity = "1";
			next_arrow.style.cursor = "pointer";
		}
	}
}

function firstRemoveLeftArrow() {
	var first_prev_arrow = document.getElementById("prev-0");
	first_prev_arrow.style.opacity = "0";
}

function displayMobileCalendar() {
	var calendar_modal = document.getElementsByClassName("single-right-wrapper")[0];
	calendar_modal.style.display = "inline";
	var calendar_close = document.getElementById("calendar_close");
	calendar_close.style.display = "inline-block";
}