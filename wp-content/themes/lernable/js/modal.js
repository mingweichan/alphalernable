// This is a file which includes functions that:
/*
  - control the function of the dialog box
*/

// Get the form modal
var fmodal = document.getElementById('modal-form');

// Get the button that opens the form modal
var btn = document.getElementById("contact-button");

// Get the <span> (x) element that closes the modal
var fclose = document.getElementById("close-form");

var tclose = document.getElementById("close-thanks");

var backToListing = document.getElementById('back-to-listing');

var tmodal = document.getElementById('modal-thanks');

// When the user clicks on the button, open the modal 
btn.onclick = function(){
    fmodal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
fclose.onclick = function(){
    fmodal.style.display = "none";
}

tclose.onclick = function(){
    tmodal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event){
    if (event.target == fmodal ) {
        fmodal.style.display = "none";
    }
}

backToListing.onclick = function(){
    tmodal.style.display = "none";
}