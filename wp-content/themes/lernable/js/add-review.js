var next = document.getElementById("review-next");
var author = document.getElementsByClassName("comment-form-author")[0];
var email = document.getElementsByClassName("comment-form-email")[0];
var rating = document.getElementsByClassName("comment-form-rating")[0];
var comment = document.getElementsByClassName("comment-form-comment")[0];

var private = document.getElementById("private-feedback-page");
var lernable = document.getElementById("lernable-feedback-page");
var submit = document.getElementsByClassName("form-submit")[0];
var optional = document.getElementsByClassName("optional-feedback-button")[0];

var improvement_buttons = document.getElementsByClassName("improvement-buttons");

next.onclick = function() {
	next.style.display = "none";
	author.style.display = "none";
	email.style.display = "none";
	rating.style.display = "none";
	comment.style.display = "none";
	
	private.style.display = "inline-block";
	submit.style.display = "block";
}

optional.onclick = function() {
	private.style.display = "none";
	lernable.style.display = "inline-block";
}

var improvement_status = [0,0,0,0,0];

improvement_buttons[0].onclick = function() {
	
	if(improvement_status[0] == 0) {
		improvement_buttons[0].style.backgroundColor = "#5DC6BB";
		improvement_buttons[0].style.color = "white";
		improvement_buttons[0].style.borderColor = "#5DC6BB";
		improvement_status[0] = 1;
	} else {
		improvement_buttons[0].style.backgroundColor = "transparent";
		improvement_buttons[0].style.color = "black";
		improvement_buttons[0].style.borderColor = "black";
		improvement_status[0] = 0;
	}
}
improvement_buttons[1].onclick = function() {
	
	if(improvement_status[1] == 0) {
		improvement_buttons[1].style.backgroundColor = "#5DC6BB";
		improvement_buttons[1].style.color = "white";
		improvement_buttons[1].style.borderColor = "#5DC6BB";
		improvement_status[1] = 1;
	} else {
		improvement_buttons[1].style.backgroundColor = "transparent";
		improvement_buttons[1].style.color = "black";
		improvement_buttons[1].style.borderColor = "black";
		improvement_status[1] = 0;
	}
}
improvement_buttons[2].onclick = function() {
	
	if(improvement_status[2] == 0) {
		improvement_buttons[2].style.backgroundColor = "#5DC6BB";
		improvement_buttons[2].style.color = "white";
		improvement_buttons[2].style.borderColor = "#5DC6BB";
		improvement_status[2] = 1;
	} else {
		improvement_buttons[2].style.backgroundColor = "transparent";
		improvement_buttons[2].style.color = "black";
		improvement_buttons[2].style.borderColor = "black";
		improvement_status[2] = 0;
	}
}
improvement_buttons[3].onclick = function() {
	
	if(improvement_status[3] == 0) {
		improvement_buttons[3].style.backgroundColor = "#5DC6BB";
		improvement_buttons[3].style.color = "white";
		improvement_buttons[3].style.borderColor = "#5DC6BB";
		improvement_status[3] = 1;
	} else {
		improvement_buttons[3].style.backgroundColor = "transparent";
		improvement_buttons[3].style.color = "black";
		improvement_buttons[3].style.borderColor = "black";
		improvement_status[3] = 0;
	}
}
improvement_buttons[4].onclick = function() {
	
	if(improvement_status[4] == 0) {
		improvement_buttons[4].style.backgroundColor = "#5DC6BB";
		improvement_buttons[4].style.color = "white";
		improvement_buttons[4].style.borderColor = "#5DC6BB";
		improvement_status[4] = 1;
	} else {
		improvement_buttons[4].style.backgroundColor = "transparent";
		improvement_buttons[4].style.color = "black";
		improvement_buttons[4].style.borderColor = "black";
		improvement_status[4] = 0;
	}
}