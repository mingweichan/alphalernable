jQuery(document).ready(function( $ ) {

    $("button.hamburger-cross").hide();
    $("div.hamburger-menu").hide();
    $("button.hamburger").click(function () {
        //$("div.hamburger-menu").slideToggle("fast", function () {
            $("div.hamburger-menu").show();
            $("button.hamburger").hide();
            $("button.hamburger-cross").show();
        //});
        $("header.site-header").css("background-image", "none");
    });

    $("button.hamburger-cross").click(function () {
//        $("div.hamburger-menu").slideToggle("slow", function () {
            $("div.hamburger-menu").hide();
            $("button.hamburger-cross").hide();
            $("button.hamburger").show();
        //});
        $("header.site-header").css("background-image", 'url("https://lernable.com/wp-content/themes/lernable/images/navbar_background.png")');
    });

});