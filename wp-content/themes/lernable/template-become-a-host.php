<?php
/**
 * The template for displaying full width pages.
 *
 * Template Name: Become a Host
 *
 * @package lernable
 */

get_header(); ?>
    <div>
        <div class="home-hero become-a-host">
            <div class="hero-button-container">
                <div class="summary-bubble">
                    <p class="small-caps">BECOME A MENTOR</p>
                    <p>Be a part of the Lernable family.</p>
                    <div class="call-to-action"> <a href="https://docs.google.com/forms/d/e/1FAIpQLSckK97pB689D-SJJwHCmkWHLfQk0B0S7gG0tAZvfVo76wPzvw/viewform" class="hero-description" target="_blank">apply now</a> </div>
                </div>
            </div>
        </div>
        <div id="primary" class="content-area">
            <div id="main" class="site-main margin become-a-host" role="main">
                <h2 class="top-h2">Mentors; let’s get you set up.</h2>
                <div class="illo-container"> <img class="illo" src="https://lernable.com/wp-content/uploads/2017/10/svgexport.png" alt="become a mentor illustration"> </div>
                <div class="all-steps-container">
                    <div id="steps-first" class="steps-container"> <img class="one" src="https://lernable.com/wp-content/uploads/2017/06/one.png" alt="one">
                        <div class="text-p">
                            <h2>Everyone’s favourite f-word: forms!</h2>
                            
                        </div>
                        <p>I’m sorry - I promise that’s the only one. You already know a bit about us; so now let’s get to know you.</p>
                        <div class="call-to-action"> <a href="https://docs.google.com/forms/d/e/1FAIpQLSckK97pB689D-SJJwHCmkWHLfQk0B0S7gG0tAZvfVo76wPzvw/viewform" class="hero-description" target="_blank">apply now</a> </div>
                    </div>
                    <div id="steps-second" class="steps-container"> <img class="two" src="https://lernable.com/wp-content/uploads/2017/09/two1.png" alt="two">
                        <div class="text-p">
                            <h2>We get to meet you!</h2>
                            
                        </div>
                        <p>We want to know what makes you giggle, what makes you cringe, and learn about what you love and how we can get people to love it too. </p><img class="second-illo mobile" src="https://lernable.com/wp-content/uploads/2017/09/asset-1.png" alt="second illustrtaion"> </div>
                    <div class="steps-container"> <img class="three" src="https://lernable.com/wp-content/uploads/2017/09/three.png" alt="three">
                        <div class="text-p">
                            <h2>The fun part. Practice lesson.</h2>
                            
                        </div> <p>You take the wheel and personally run us through a test lesson. We learn a thing or two, give you some tips we’ve picked up from other mentors and make sure you’re prepared for anything! </p><img class="third-illo" src="https://lernable.com/wp-content/uploads/2017/09/meetinghost.png" alt="third illustrtaion"> </div>
                    <div class="steps-container"> <img class="three" src="https://lernable.com/wp-content/uploads/2017/09/three.png" alt="three">
                        <div id="show-up" class="text-p">
                            <h2>Some finishing touches…</h2>
                           
                        </div>
                         <p>We take some photos, write up your listing, and start looking for learners for you to teach! </p>
                    </div>
                </div>
                <!--
            <div class="steps-container"> <img class="one" src="https://lernable.com/wp-content/uploads/2017/06/one.png" alt="one">
                <div class="text-p">
                    <h2>Everyone’s favourite f-word: forms!</h2>
                    <p>I’m sorry - I promise that’s the only one.
                        <br> You already know a bit about us; so now let’s get to know you.
                        <br> Here is a quick form just so we can grab some basic information on what your about and how we can get in touch with you. </p>
                </div> <img class="first-illo" src="https://lernable.com/wp-content/uploads/2017/10/form.png" alt="form first illustrtion"> </div>
            <div class="steps-container become-illo2">
                 <img class="secondb-illo" src="https://lernable.com/wp-content/uploads/2017/10/guitarsewing.png" alt="form second b illustrtion"> <img class="second-illo" src="https://lernable.com/wp-content/uploads/2017/10/cookingdude.png" alt="form second illustrtion"><br>
                <div class="text-p-container">
                <img class="two" src="https://lernable.com/wp-content/uploads/2017/09/two1.png" alt="two">
                
                 </div></div>
            <div class="steps-container">
                <div class="text-p">
                    <h2>We get to meet you!</h2>
                    <p>We want to know what makes you giggle, what makes you cringe, and learn about what you love and how we can get people to love it too.  </p>
                    </div>
            </div>
--></div>
        </div>
        <div class="home-hero become-a-host2">
            <h2>Just start.</h2>
            <div class="call-to-action"> <a href="https://docs.google.com/forms/d/e/1FAIpQLSckK97pB689D-SJJwHCmkWHLfQk0B0S7gG0tAZvfVo76wPzvw/viewform" class="hero-description" target="_blank">apply now</a> </div>
        </div>
        <?php
get_footer();