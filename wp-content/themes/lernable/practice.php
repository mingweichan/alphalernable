<div class="lern-tab">
    <button id="defaultOpen" class="tablinks" onclick="openTab(event,'single')">Single lesson</button><button class="tablinks" onclick="openTab(event,'course')">5 lesson course</button>
</div>
<div id="single" class="tabcontent">
    <div class="people-duration">
        <div><i class="fa fa-user" aria-hidden="true"></i></div>
        <p class="next-to-icon">2 people max</p><br/>
    </div>
    <div class="people-duration">
        <div><i class="fa fa-clock-o" aria-hidden="true"></i> </div>
        <p class="next-to-icon">2 hours/lesson</p>

    </div>
    <div class="single-listing-details">
        <p class="font-reg">Your skill level</p>
        Beginner
        <p class="font-reg">What you'll learn</p>
        - Basic foundation and balance along with some simple tricks to take to the park
        <p class="font-reg">What's included</p>
        - Skateboard 
        - Protective gear
        <p class="font-reg">Where</p>
        Brick Pit, Thornleigh, Australia
        <a href="https://www.google.com.au/maps/place/Roseville+NSW+2069">
            <div class="map-img"><img class="alignnone size-medium wp-image-106" src="https://maps.googleapis.com/maps/api/staticmap?center=Roseville+NSW+2069&markers=&zoom=12&size=600x300&maptype=roadmap&key=AIzaSyB7jbvyEf2aAUCZDDrokFicpFJpJ1a_its" alt="" /></div>
        </a>
        <p class="font-reg">What to bring</p>
        ID that matches account details for proof of identity

        <p class="font-reg">Refund policy</p>
        If you would like to cancel within 24 hours of booking, a full refund will be issued

    </div>
</div>
<div id="course" class="tabcontent">


    <div class="people-duration">
        <div><i class="fa fa-user" aria-hidden="true"></i></div>
        <p class="next-to-icon">2 people max</p><br/>
    </div>
    <div class="people-duration">
        <div><i class="fa fa-clock-o" aria-hidden="true"></i> </div>
        <p class="next-to-icon">2 hours/lesson</p>

    </div>
    <div class="single-listing-details">
        <p class="font-reg">Your skill level</p>
        Beginner
        <p class="font-reg">What you'll learn</p>
        - Stance on the board and techniques of balance
        - Carving the road and fundamentals of turning
        - The Ollie
        - How to ride a ramp
        <p class="font-reg">What's included</p>
        - Skateboard 
        - Protective gear
        <p class="font-reg">Where</p>
        Brick Pit, Thornleigh, Australia
        <a href="https://www.google.com.au/maps/place/Roseville+NSW+2069">
            <div class="map-img"><img class="alignnone size-medium wp-image-106" src="https://maps.googleapis.com/maps/api/staticmap?center=Roseville+NSW+2069&markers=&zoom=12&size=600x300&maptype=roadmap&key=AIzaSyB7jbvyEf2aAUCZDDrokFicpFJpJ1a_its" alt="" /></div>
        </a>
        <p class="font-reg">What to bring</p>
        ID that matches account details for proof of identity

        <p class="font-reg">Refund policy</p>
        If you would like to cancel within 24 hours of booking, a full refund will be issued


    </div>

    <script type="text/javascript">
        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();
    </script>