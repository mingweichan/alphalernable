<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

?>

		</div><!-- .col-full -->
	</div><!-- #content -->

	<?php do_action( 'storefront_before_footer' ); ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		
		<div class="footer-links">
			<div class="link"><a href="https://lernable.com/terms">Terms</a></div>
			<div class="link"><a href = "https://lernable.com/privacy">Privacy</a></div>
		</div>
		
		<div class="social-media">
			<a class="not-last" href="https://www.facebook.com/Lernable/"><i class="fa fa-facebook"></i></a>
			<a class="not-last" href="https://twitter.com/lernable/"><i class="fa fa-twitter"></i></a>
			<a class="not-last" href="https://www.instagram.com/lernable/"><i class="fa fa-instagram"></i></a>
			<a href="mailto:contact@lernable.com"><i class="fa fa-envelope"></i></a>	
		</div>
		
		<div class="copyright">
            <p>© Lernable 2017</p>
		</div>
	</footer><!-- #colophon -->

	<?php do_action( 'storefront_after_footer' ); ?>

   <script type="text/javascript" src="https://lernable.com/wp-includes/js/jquery/jquery.js"></script>
   <script type="text/javascript" src="https://lernable.com/wp-content/themes/lernable/js/fontAwesome.js"></script>
   <script type="text/javascript" src="https://lernable.com/wp-content/themes/lernable/js/hamburger.js"></script> 
<!--<script type="text/javascript" src="https://lernable.com/wp-content/themes/lernable/js/fadein.js"></script> -->
<script type="text/javascript" src="https://lernable.com/wp-content/themes/lernable/js/braintree.js"></script>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>