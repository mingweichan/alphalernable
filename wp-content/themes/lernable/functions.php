<?php

remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );


add_action( 'after_setup_theme', 'remove_pgz_theme_support', 100 );

function remove_pgz_theme_support() { 
remove_theme_support( 'wc-product-gallery-zoom' );
}
// make function not do anything
	function storefront_product_search() {
/*		if ( storefront_is_woocommerce_activated() ) { ?>
    //
    <div class="site-search">
        //
        <?php the_widget ( 'WC_Widget_Product_Search', 'title=' ) ; ?> // </div>
    // */ } 

add_action( 'wp_enqueue_scripts', 'wpsites_google_fonts' );
function wpsites_google_fonts() {
wp_enqueue_style( 'gfonts', '//fonts.googleapis.com/css?family=Oswald|Open+Sans', array() );
}

add_filter( 'woocommerce_star_rating_filter', 'return_stars' );
function return_stars( $prodid ) {
// Your code
return $prodid;
echo $prodid;
}

// Override theme default specification for product # per row
function loop_columns() {
return 4; // 5 products per row
}
add_filter('loop_shop_columns', 'loop_columns', 999);

//LISTING STUFF



//SWITCHING PRICE AND RATING IN LISTING DISPLAY
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 10 );
add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 5 );
//REMOVING TITLE AND PRICE AND BOOK IN PRODUCT SUMMARY
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 6);
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 8);
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 7);
//ADD RATING TO AFTER SIDEBAR IMAGE IN SINGLE PRODUCT
//add_action( 'lernable_after_product_image', 'woocommerce_template_single_rating', 5 );
//NEW FUNCTION FOR BOOKING DATES IN SINGLE PRODUCT
//add_action( 'woocommerce_before_single_product_summary', 'lernable_book', 30 );


function woocommerce_product_subcategories( $args = array() ) {
		global $wp_query;

		$defaults = array(
			'before'        => '',
			'after'         => '',
			'force_display' => false,
		);

		$args = wp_parse_args( $args, $defaults );

		extract( $args );

		// Main query only
		if ( ! is_main_query() && ! $force_display ) {
			return;
		}

		// Don't show when filtering, searching or when on page > 1 and ensure we're on a product archive
		if ( is_search() || is_filtered() || is_paged() ) {
			return;
		}

		// Check categories are enabled
		if ( is_shop() && '' === get_option( 'woocommerce_shop_page_display' ) ) {
			return;
		}

		// Find the category + category parent, if applicable
		$term 			= get_queried_object();
		$parent_id 		= empty( $term->term_id ) ? 0 : $term->term_id;

		if ( is_product_category() ) {
			$display_type = get_woocommerce_term_meta( $term->term_id, 'display_type', true );

			switch ( $display_type ) {
				case 'products' :
					return;
				break;
				case '' :
					if ( '' === get_option( 'woocommerce_category_archive_display' ) ) {
						return;
					}
				break;
			}
		}

		// NOTE: using child_of instead of parent - this is not ideal but due to a WP bug ( https://core.trac.wordpress.org/ticket/15626 ) pad_counts won't work
		$product_categories = get_categories( apply_filters( 'woocommerce_product_subcategories_args', array(
			'parent'       => $parent_id,
			'menu_order'   => 'ASC',
			'hide_empty'   => 0,
			'hierarchical' => 1,
			'taxonomy'     => 'product_cat',
			'pad_counts'   => 1,
		) ) );

		if ( apply_filters( 'woocommerce_product_subcategories_hide_empty', true ) ) {
			$product_categories = wp_list_filter( $product_categories, array( 'count' => 0 ), 'NOT' );
		}

		if ( $product_categories ) {
			echo $before;

			foreach ( $product_categories as $category ) {
				wc_get_template( 'content-product_cat.php', array(
					'category' => $category,
				) );
			}

			// If we are hiding products disable the loop and pagination
			if ( is_product_category() ) {
				$display_type = get_woocommerce_term_meta( $term->term_id, 'display_type', true );

				switch ( $display_type ) {
					case 'subcategories' :
						$wp_query->post_count    = 0;
						$wp_query->max_num_pages = 0;
					break;
					case '' :
						if ( 'subcategories' === get_option( 'woocommerce_category_archive_display' ) ) {
							$wp_query->post_count    = 0;
							$wp_query->max_num_pages = 0;
						}
					break;
				}
			}

			if ( is_shop() && 'subcategories' === get_option( 'woocommerce_shop_page_display' ) ) {
				$wp_query->post_count    = 0;
				$wp_query->max_num_pages = 0;
			}

			echo $after;

			return true;
		}
	}


//CLEAR CART
add_action( 'init', 'woocommerce_clear_cart_url' );
function woocommerce_clear_cart_url() {
  global $woocommerce;

    if ( is_page('Front Page') && isset( $_GET['empty-cart'] ) ) { 
        $woocommerce->cart->empty_cart(); 
    }
}

if ( ! function_exists( 'storefront_page_header' ) ) {
	/**
	 * Display the post header with a link to the single post
	 *
	 * @since 1.0.0
	 */
	function storefront_page_header() {
		?>
        <header class="entry-header">
            <?php
			storefront_post_thumbnail( 'full' );
			// the_title( '<h1 class="entry-title">', '</h1>' ); // just got rid of this line
			?>
        </header>
        <!-- .entry-header -->
        <?php
	}
}
/*
function woocommerce_variable_add_to_cart(){
    global $product, $post;
 
    $variations = find_valid_variations();
 
    // Check if the special 'price_grid' meta is set, if it is, load the default template:
    if ( get_post_meta($post->ID, 'price_grid', true) ) {
        // Enqueue variation scripts
        wp_enqueue_script( 'wc-add-to-cart-variation' );
 
        // Load the template
        wc_get_template( 'single-product/add-to-cart/variable.php', array(
                'available_variations'  => $product->get_available_variations(),
                'attributes'            => $product->get_variation_attributes(),
                'selected_attributes'   => $product->get_variation_default_attributes()
            ) );     
        return;
    }
    ?>
            <table class="variations variations-grid" cellspacing="0">
                <tbody>
                    <?php
            $variation_attributes = array();
            $variation_attributes = $product->get_variation_attributes();
            $stack = array();
    
            foreach ($variations as $key => $value) {
                $date_and_time = $value['attributes']['attribute_year'].'-'.$value['attributes']['attribute_month'].'-'.$value['attributes']['attribute_day'].'-'.$value['attributes']['attribute_time'];
                if( in_array($date_and_time,$stack) ) continue; //prevent repeat variations
                $stack[] = $date_and_time;
                if( !$value['variation_is_visible'] ) continue;
            ?>
                        <?php if( $value['is_in_stock'] ) { ?>
                        <tr>
                            <td>
                                <div class="attribute_name">
                                    <?php
                            
                            $val = str_replace(array('-','_'), ' ', $date_and_time);
                            printf( '<span class="attr attr-attribute_date_and_time">%s</span>', ucwords($val) );
                     ?>
                                    <?php //if( $value['is_in_stock'] ) { ?>
                                </div>
                                <div class="booking_button">
                                    <form class="cart" action="<?php echo esc_url( $product->add_to_cart_url() ); ?>" method="post" enctype='multipart/form-data'>
                                        <?php // woocommerce_quantity_input(); ?>
                                        <?php
                        if(!empty($value['attributes'])){
                            foreach ($value['attributes'] as $attr_key => $attr_value) {
                            ?>
                                            <input type="hidden" name="<?php echo $attr_key?>" value="<?php echo $attr_value?>">
                                            <?php
                            }
                        }
                        ?>
                                                <button type="submit" class="booking_button">Book</button>
                                                <input type="hidden" name="variation_id" value="<?php echo $value['variation_id']?>" />
                                                <input type="hidden" name="product_id" value="<?php echo esc_attr( $post->ID ); ?>" />
                                                <input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $post->ID ); ?>" />
                                    </form>
                                </div>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php } ?>
                </tbody>
            </table>
            <?php
}
*/

function find_valid_variations() {
    global $product;
 
    $variations = $product->get_available_variations();
    $attributes = $product->get_attributes();
    $new_variants = array();
 
    // Loop through all variations
    foreach( $variations as $variation ) {
 
        // Peruse the attributes.
 
        // 1. If both are explicitly set, this is a valid variation
        // 2. If one is not set, that means any, and we must 'create' the rest.
 
        $valid = true; // so far
        foreach( $attributes as $slug => $args ) {
            if( array_key_exists("attribute_$slug", $variation['attributes']) && !empty($variation['attributes']["attribute_$slug"]) ) {
                // Exists
 
            } else {
                // Not exists, create
                $valid = false; // it contains 'anys'
                // loop through all options for the 'ANY' attribute, and add each
                foreach( explode( '|', $attributes[$slug]['value']) as $attribute ) {
                    $attribute = trim( $attribute );
                    $new_variant = $variation;
                    $new_variant['attributes']["attribute_$slug"] = $attribute;
                    $new_variants[] = $new_variant;
                }
 
            }
        }
 
        // This contains ALL set attributes, and is itself a 'valid' variation.
        if( $valid )
            $new_variants[] = $variation;
 
    }
 
    return $new_variants;
}

add_filter( 'wc_add_to_cart_message_html', 'remove_add_to_cart_message' );
function remove_add_to_cart_message() {
    return;
}

/**
 * Trim zeros in price decimals
 **/
 add_filter( 'woocommerce_price_trim_zeros', '__return_true' );

if ( ! function_exists( 'wc_create_new_customer' ) ) {

	/**
	 * Create a new customer.
	 *
	 * @param  string $email Customer email.
	 * @param  string $username Customer username.
	 * @param  string $password Customer password.
	 * @return int|WP_Error Returns WP_Error on failure, Int (user ID) on success.
	 */
	function wc_create_new_customer( $email, $username = '', $password = '' ) {

		// Check the email address.
		if ( empty( $email ) || ! is_email( $email ) ) {
			return new WP_Error( 'registration-error-invalid-email', __( 'Please provide a valid email address.', 'woocommerce' ) );
		}

		if ( email_exists( $email ) ) {
			return new WP_Error( 'registration-error-email-exists', __( 'An account is already registered with your email address. Please login.', 'woocommerce' ) );
		}

		// Handle username creation.
//		if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) || ! empty( $username ) ) {
			$username = sanitize_user( $username );

			if ( empty( $username ) || ! validate_username( $username ) ) {
				return new WP_Error( 'registration-error-invalid-username', __( 'Please enter a valid account username.', 'woocommerce' ) );
			}
       // Check no numbers in name
         if(1 === preg_match('~[0-9]~', $username) ) {
             return new WP_Error( 'registration-error-invalid-username', __('Please enter a name without numbers.', 'woocommerce' ) );
         }
       
/*			if ( username_exists( $username ) ) {
				return new WP_Error( 'registration-error-username-exists', __( 'An account is already registered with that username. Please choose another.', 'woocommerce' ) );
			}*/
//		} else {
//			$username = sanitize_user( current( explode( '@', $email ) ), true );

			// Ensure username is unique.
			$append     = 1;
			$o_username = $username;

			while ( username_exists( $username ) ) {
				$username = $o_username . $append;
				$append++;
			}

		// Handle password creation.
		if ( 'yes' === get_option( 'woocommerce_registration_generate_password' ) && empty( $password ) ) {
			$password           = wp_generate_password();
			$password_generated = true;
		} elseif ( empty( $password ) ) {
			return new WP_Error( 'registration-error-missing-password', __( 'Please enter an account password.', 'woocommerce' ) );
		} else {
			$password_generated = false;
		}

		// Use WP_Error to handle registration errors.
		$errors = new WP_Error();

		do_action( 'woocommerce_register_post', $username, $email, $errors );

		$errors = apply_filters( 'woocommerce_registration_errors', $errors, $username, $email );

		if ( $errors->get_error_code() ) {
			return $errors;
		}

		$new_customer_data = apply_filters( 'woocommerce_new_customer_data', array(
			'user_login' => $username,
			'user_pass'  => $password,
			'user_email' => $email,
			'role'       => 'customer',
		) );

		$customer_id = wp_insert_user( $new_customer_data );

		if ( is_wp_error( $customer_id ) ) {
			return new WP_Error( 'registration-error', '<strong>' . __( 'Error:', 'woocommerce' ) . '</strong> ' . __( 'Couldn&#8217;t register you&hellip; please contact us if you continue to have problems.', 'woocommerce' ) );
		}

		do_action( 'woocommerce_created_customer', $customer_id, $new_customer_data, $password_generated );

		return $customer_id;
	}
}

//ADD ACTION TO TITLE BLOCK (LOCATION OF LESSON AND TAUGHT IN WHAT LANGUAGES)
add_action( 'woocommerce_single_product_summary', 'lernable_location_languages', 10 );

function lernable_location_languages() {
    global $product;
    echo '<div class="location-languages">';
        echo '<p class="location">' . $product->get_attribute( 'location' ) . '</p>';
        echo '<p class="languages">Taught in ' . $product->get_attribute( 'languages' ) . '</p>';
    echo '</div>';
}

add_action( 'woocommerce_single_product_summary', 'lernable_contact_text', 9);
function lernable_contact_text() {
    global $product;
    $name = $product->get_attribute( 'name' );
    $sex = $product->get_attribute( 'sex' );
    echo '<p class="contact-button-before">Got a question for ' . $name . '? <a class="contact-button" id="contact-button">Contact ' . $sex . '.</a></p>';
}


if ( ! function_exists( 'woocommerce_template_single_price' ) ) {

	/**
	 * Output the product price.
	 *
	 * @subpackage	Product
     * also out put max size and duration
	 */
	function woocommerce_template_single_price() {
		wc_get_template( 'single-product/price.php' );
        global $product;
        $size_of_lesson = $product->get_attribute('size_of_lesson');
        $duration = $product->get_attribute('duration');
        $duration_string = substr($duration, 0, -1);
    echo '<div class="people-duration">
        <div><i class="fa fa-user" aria-hidden="true"></i></div>
        <p class="next-to-icon">'. $size_of_lesson . 
      ' person max</p>
    &nbsp;
    </div>
    <div class="people-duration">
        <div><i class="fa fa-clock-o" aria-hidden="true"></i> </div>
        <p class="next-to-icon">';
    echo $duration_string;
    if( substr( $duration, -1 ) == "m" ) {
        echo ' minutes';
    } else {
        if (intval($duration_string) == 1) {
            echo ' hour';
        } else {
            echo ' hours';
        }
    }
    echo '</p></div>';
	}
}

class Calendar {  
     
    /**
     * Constructor
     */
    public function __construct(){     
        $this->naviHref = htmlentities($_SERVER['PHP_SELF']);
    }
     
    /********************* PROPERTY ********************/  
    private $dayLabels = array("M","T","W","T","F","S","S");
     
    private $currentYear=0;
     
    private $currentMonth=0;
     
    private $currentDay=0;
     
    private $currentDate=null;
     
    private $daysInMonth=0;
     
    private $naviHref= null;
     
    /********************* PUBLIC **********************/  
        
    /**
    * print out the calendar
    */
    public function show($next_month) {

        date_default_timezone_set( 'Australia/Sydney' );
        
        $month_string = 'first day of +' . $next_month . 'month';
        $year = date("Y",strtotime($month_string));  
        $month = date("m",strtotime($month_string));
           
        $this->currentYear=$year;
         
        $this->currentMonth=$month;
         
        $this->daysInMonth=$this->_daysInMonth($month,$year);  
         
        $content='<div class="calendar" id="calendar-'. $next_month  .'"';
        
        if($next_month!=0) {
            $content .= ' style="display:none;"';
        }
        $content .= '>'.
                        '<div class="box">'.
                        $this->_createNavi($next_month).
                        '</div>'.
                        '<div class="box-content">'.
                                '<ul class="label">'.$this->_createLabels().'</ul>';   
                                $content.='<div class="clear"></div>';     
                                $content.='<ul class="dates">';
                                 
                                $weeksInMonth = $this->_weeksInMonth($month,$year);
                                // Create weeks in a month
                                for( $i=0; $i<$weeksInMonth; $i++ ){
                                     
                                    // Create days in a week
                                    for($j=1;$j<=7;$j++){
                                        $content.=$this->_showDay($i*7+$j, $next_month);
                                    }
                                }
                                 
                                $content.='</ul>';
             
                        $content.='</div>';
                 
        $content.='</div>';
        echo $content;
        $this->_createLessons($next_month);
    }
    
    /********************* PRIVATE **********************/ 

    private function _createLessons($next_month) {
        
        date_default_timezone_set( 'Australia/Sydney' );
        echo '<div id="lessons">';
        $day_names = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
        $month_names = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        
        global $product, $post;
        $content = array('<div id="lesson-block-'.$next_month.'-1" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-2" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-3" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-4" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-5" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-6" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-7" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-8" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-9" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-10" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-11" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-12" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-13" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-14" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-15" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-16" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-17" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-18" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-19" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-20" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-21" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-22" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-23" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-24" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-25" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-26" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-27" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-28" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-29" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-30" class="lesson-block" style="display:none;">',
                         '<div id="lesson-block-'.$next_month.'-31" class="lesson-block" style="display:none;">');
        
        $firstDayOfTheWeek = date('N',strtotime($this->currentYear.'-'.$this->currentMonth.'-01'));
        $index = 0;
        $month_names_index = date('n',strtotime($this->currentYear.'-'.$this->currentMonth.'-01'));
        $ordinal_endings = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
        
        while($index + 1 <= 31) {
            $day_names_index = ($index + 1 + intval($firstDayOfTheWeek) - 1)%7;
            if($index + 1 == '11' || $index + 1 == '12' || $index + 1 == '13') {
                $ordinal_ending = 'th';
            } else {
                $ordinal_ending = $ordinal_endings[($index+1)%10];
            }
            
            $content[$index] .= '<div class="lesson-block-content">';
            $content[$index] .= '<div class="lesson-block-title">'. $day_names[$day_names_index] .', '. strval($index + 1) . $ordinal_ending . ' ' . $month_names[$month_names_index-1] . ' ' . $this->currentYear . '</div>';
            $index += 1;
        }
        $stack = array();        
        
        // don't display variation if the date has already passed current time
        $year = date("Y",time()); 
        $month = date("n",time());
        $day = date("j",time());
        
        $variations = find_valid_variations();
        foreach ($variations as $key => $value) {
            if( intval($value['attributes']['attribute_year']) < intval($year) ) continue;
            if( intval($value['attributes']['attribute_year']) == intval($year) && intval($value['attributes']['attribute_month']) < intval($month) ) continue;
            if (intval($value['attributes']['attribute_year']) == intval($year) && intval($value['attributes']['attribute_month']) == intval($month) && intval($value['attributes']['attribute_day']) < intval($day)) continue;
            
            if( !$value['is_in_stock'] ) continue;
            if( intval($value['attributes']['attribute_year']) != intval($this->currentYear)) continue;
            if( intval($value['attributes']['attribute_month']) != intval($this->currentMonth)) continue;
            if( !$value['variation_is_visible'] ) continue;
            $date_and_time = $value['attributes']['attribute_year'].'-'.$value['attributes']['attribute_month'].'-'.$value['attributes']['attribute_day'].'-'.$value['attributes']['attribute_time'];
            if( in_array($date_and_time,$stack) ) continue; // prevent repeat variations
            $stack[] = $date_and_time;
            
            $attribute_day_index = intval($value['attributes']['attribute_day']) - 1;
            $duration_string = $product->get_attribute('duration');
            $duration = substr($duration_string, 0, -1);
            $min_or_hr = substr($duration_string, -1);
            if( $min_or_hr == "m" ) {
                $duration_hour = $duration / 60;
                $duration_minute = ':'. sprintf("%02d", $duration % 60);
            } else {
                $duration_hour = $duration;
                $duration_minute = ':00';
            }
            
            $end_time = intval($value['attributes']['attribute_time']) + intval($duration_hour);
            $meridiem = $value['attributes']['attribute_meridiem'];
            $end_meridiem = ' ' . $meridiem;
            if($end_time > 24) $end_time %= 24;
            if($end_time > 12 and intval( $value['attributes']['attribute_time']) != 12 ) {
                $end_time %= 12;
                if($meridiem == 'am') {
                    $end_meridiem = ' pm';
                } else {
                    $end_meridiem = ' am';
                }
            }
            
            $attribute_time = $value['attributes']['attribute_time'] .':00 ';
            if($end_meridiem != (' ' . $meridiem) ) {
				    $attribute_time .= $meridiem;
				}
            $attribute_time .= ' - '. $end_time . $duration_minute . $end_meridiem;
            
            $content[$attribute_day_index] .= 
                '<div class="indiv-lesson">'.
                    '<span class="attr attr-attribute_date_and_time">'. $attribute_time . '</span>'.
                    '<div class="booking-button">'.
                        '<form class="cart" action="'. esc_url( $product->add_to_cart_url() ) .'" method="post" enctype="multipart/form-data">';
                            foreach ($value['attributes'] as $attr_key => $attr_value) {
                                    $content[$attribute_day_index] .= '<input type="hidden" name="'. $attr_key .'" value="'. $attr_value .'">';
                            }
                        $content[$attribute_day_index] .=
                            '<button type="submit" class="booking_button">Book</button>'.
                            '<input type="hidden" name="variation_id" value="'. $value['variation_id'] .'" />'.
                            '<input type="hidden" name="product_id" value="'. esc_attr( $post->ID ) .'" />'.
                            '<input type="hidden" name="add-to-cart" value="'. esc_attr( $post->ID ) .'" />'.
                        '</form>'.
                    '</div>'.
                '</div>'.
                '<span id="has_lessons-' . $next_month . '-' . ltrim($value['attributes']['attribute_day'], '0') .'"></span>';
        }

        $index = 0;
        while ($index + 1 <= intval($this->daysInMonth)) {
            $content[$index] .= '</div></div>';
            echo $content[$index];
            $index += 1;
        }
        echo '</div>';
    }
    
    /**
    * create the li element for ul
    */
    private function _showDay($cellNumber, $next_month){
        $notInMonth = 0;
        if($this->currentDay==0){
             
            $firstDayOfTheWeek = date('N',strtotime($this->currentYear.'-'.$this->currentMonth.'-01'));
            
            if(intval($cellNumber) == intval($firstDayOfTheWeek)){
                $this->currentDay=1;
            } else {
                $this->currentDate = null;
                $previousMonth = intval($this->currentMonth);
                $currYear = intval($this->currentYear);
                if($previousMonth == 1) {
                    $previousMonth = 12;
                    $currYear -= 1;
                } else {
                    $previousMonth -= 1;
                }
                $daysInMonth = cal_days_in_month(CAL_GREGORIAN, $previousMonth, $currYear);
                $cellContent = strval($daysInMonth - intval($firstDayOfTheWeek) + intval($cellNumber) + 1);
                $notInMonth = 1;
            }
        }
         
        if( ($this->currentDay!=0)&&($this->currentDay<=$this->daysInMonth) ){
             
            $this->currentDate = date('Y-m-d',strtotime($this->currentYear.'-'.$this->currentMonth.'-'.($this->currentDay)));
            
            $cellContent = $this->currentDay;
            $this->currentDay++;
             
        } else if($this->currentDay>$this->daysInMonth){
            
            $lastDayInMonth = date('N',strtotime($this->currentYear.'-'.$this->currentMonth.'-'.$this->daysInMonth));
            $weekCellNumber = $cellNumber%7;
            if($weekCellNumber == 0) {
                $weekCellNumber = 7;
            }
            $cellContent = strval( ($weekCellNumber) - intval($lastDayInMonth) );
            $notInMonth = 1;
        }
        $firstDayOfTheWeek = date('N',strtotime($this->currentYear.'-'.$this->currentMonth.'-01'));
        $dayNumber = strval( $cellNumber - intval($firstDayOfTheWeek) + 1 );
         
        return '<li id="li-'. $next_month .'-'. $dayNumber .'" class="'.($cellNumber%7==1?'start ':($cellNumber%7==0?'end ':'')).
                ($notInMonth==1?'not_in_curr_month':'').'" onclick="showLessonBlock('.$dayNumber.', '. $next_month .')">'.$cellContent.'<span class="available-lessons" id="available_lessons-' . $next_month . '-' . $dayNumber . '" style="display:none;"></span></li>';
    }
     
    /**
    * create navigation
    */
    private function _createNavi($next_month){
         
        $month_names = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        
        return
            '<div class="header">'.
                '<button class="prev" id="prev-'. $next_month .'" onclick="prevCal()">&#9668;</button>'.
                    '<div class="title">'. $month_names[intval(date('n', strtotime($this->currentYear.'-'.$this->currentMonth.'-1'))) - 1] .' '. date('Y',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).'</div>'.
                '<button class="next" id="next-'. $next_month .'" onclick="nextCal()">&#9658;</button>'.
            '</div>';
    }
         
    /**
    * create calendar week labels
    */
    private function _createLabels(){  
                 
        $content='';
         
        foreach($this->dayLabels as $index=>$label){
             
            $content.='<li class="'.($label==6?'end title':'start title').' title">'.$label.'</li>';
 
        }
         
        return $content;
    }
     
     
     
    /**
    * calculate number of weeks in a particular month
    */
    private function _weeksInMonth($month=null,$year=null){
         
        if( null==($year) ) {
            $year =  date("Y",time()); 
        }
         
        if(null==($month)) {
            $month = date("m",time());
        }
         
        // find number of days in this month
        $daysInMonths = $this->_daysInMonth($month,$year);
         
        $numOfweeks = ($daysInMonths%7==0?0:1) + intval($daysInMonths/7);
         
        $monthEndingDay= date('N',strtotime($year.'-'.$month.'-'.$daysInMonths));
         
        $monthStartDay = date('N',strtotime($year.'-'.$month.'-01'));
         
        if($monthEndingDay<$monthStartDay){
             
            $numOfweeks++;
         
        }
         
        return $numOfweeks;
    }
 
    /**
    * calculate number of days in a particular month
    */
    private function _daysInMonth($month=null,$year=null){
         
        if(null==($year))
            $year =  date("Y",time()); 
 
        if(null==($month))
            $month = date("m",time());
             
        return date('t',strtotime($year.'-'.$month.'-01'));
    }
     
}

if ( ! function_exists( 'lernable_book' ) ) {
    function lernable_book() {
        for($i=0; $i<4; $i++) {
            $calendar = new Calendar();
            $calendar->show($i);
        }
    }
}

/**
* Get the product thumbnail, or the placeholder if not set.
*
* @subpackage	Loop
* @param string $size (default: 'shop_catalog')
* @param int $deprecated1 Deprecated since WooCommerce 2.0 (default: 0)
* @param int $deprecated2 Deprecated since WooCommerce 2.0 (default: 0)
* @return string
*/
function woocommerce_get_product_thumbnail( $size = 'shop_catalog', $deprecated1 = 0, $deprecated2 = 0 ) {
  global $post;
  $image_size = apply_filters( 'single_product_archive_thumbnail_size', $size );

  if ( has_post_thumbnail() ) {
     $props = wc_get_product_attachment_props( get_post_thumbnail_id(), $post );
     return get_the_post_thumbnail( $post->ID, $image_size, array(
        'alt'    => $props['alt'],
     ) );
  } elseif ( wc_placeholder_img_src() ) {
     return wc_placeholder_img( $image_size );
  }
}

function unrequire_billing_fields( $fields ) {
    $fields['billing_country']['required'] = false;
    $fields['billing_address_1']['required'] = false;
    $fields['billing_city']['required'] = false;
    $fields['billing_state']['required'] = false;
    $fields['billing_postcode']['required'] = false;
    return $fields;
}
add_filter( 'woocommerce_billing_fields', 'unrequire_billing_fields' );

function change_form_row_order( $fields ) {
    $fields['billing_last_name']['class'][0] = "form-row-middle";
    $fields['billing_email']['class'][0] = "form-row-last";
    $fields['billing_phone']['class'][0] = "form-row-first";
    return $fields;
}
add_filter( 'woocommerce_billing_fields', 'change_form_row_order');

function change_billing_field_labels( $fields ) {
    $fields['billing_phone']['label'] = "Phone number";
    return $fields;
}
add_filter( 'woocommerce_billing_fields', 'change_billing_field_labels');


// OVERRIDING FRONTEND JS FILES
add_action('wp_enqueue_scripts', 'override_woo_frontend_scripts');
function override_woo_frontend_scripts() {
    wp_deregister_script('wc-checkout');
    wp_enqueue_script('wc-checkout', 'https://lernable.com/wp-content/themes/lernable/woocommerce/js/checkout.js', array('jquery', 'woocommerce', 'wc-country-select', 'wc-address-i18n'), null, true);
}

add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_duration', 8 );
function woocommerce_template_loop_duration() {
    global $product;
    $duration_string = $product->get_attribute( 'duration' );
    $duration = substr($duration_string, 0, -1);
    echo '<div class="duration">' . $duration;
    if( substr( $duration_string, -1 ) == "m" ) {
        echo ' minutes';
    } else {
        if (intval($duration) == 1) {
            echo ' hour';
        } else {
            echo ' hours';
        }
    }
    echo '</div>';
}

function filter_woocommerce_email_heading_processing( $subject , $order ) { 
	
	$items = $order->get_items();
	foreach ( $items as $item_id => $item ) {
		$lesson_name = $item->get_name();
	}
	
	$subject = sprintf( 'Lernable Booking #%s from %s - %s', $order->get_order_number(), $order->get_date_paid()->date("jS F, Y"), $lesson_name );
	
	return $subject;
}; 

function filter_woocommerce_email_heading_completed( $subject , $order ) {
	$items = $order->get_items();
	foreach ( $items as $item_id => $item ) {
		$lesson_name = $item->get_name();
		foreach ( $item->get_formatted_meta_data() as $meta_id => $meta ) {
			if($meta->{"key"} == "day") $day = $meta->{"value"};
			else if($meta->{"key"} == "month") $month = $meta->{"value"};
			else if($meta->{"key"} == "year") $year = $meta->{"value"};
		}
	}
	
	$subject = sprintf( 'Leave a review for your lesson on %s - %s', date("jS F, Y", strtotime($year.'-'.$month.'-'.$day)) , $lesson_name );
	
	return $subject;
}

// add the filter 
add_filter( "woocommerce_email_subject_customer_processing_order", 'filter_woocommerce_email_heading_processing', 1, 2 );

add_filter( "woocommerce_email_subject_customer_completed_order", 'filter_woocommerce_email_heading_completed', 1, 2 ); 

add_filter( 'comment_form_fields', 'move_comment_field_to_bottom' );
function move_comment_field_to_bottom( $fields ) {
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;
	return $fields;
}


add_filter('comment_post_redirect', 'redirect_after_comment');
function redirect_after_comment($location)
{
	$i = strlen($location);
	while( ctype_digit($location[$i-1]) ) {
		$i -= 1;
	}
	$comment_id = substr($location, $i);
	$post_id = get_comment($comment_id, ARRAY_A)['comment_post_ID'];
	
	return 'https://lernable.com/thank-you/?id=' . $post_id;
}

add_filter('wp_mail_content_type','set_content_type');

function set_content_type($content_type){
	return 'text/html';
}


?>