<?php
/**
 * The template for displaying the terms and conditions page.
 *
 * Template Name: How It Works
 *
 * @package lernable
 */

get_header(); ?>
    <div class="home-hero howitworks">
        <div class="hero-button-container">
            <div class="summary-bubble">
                <p class="small-caps">HOW IT WORKS</p>
                <p>Everything is lernable.</p>
                <p>Just start.</p>
                <div class="call-to-action"> <a href="https://www.lernable.com" class="hero-description">see available lessons</a> </div>
            </div>
        </div>
    </div>
    <div id="primary" class="content-area">
        <main id="main" class="site-main margin" role="main">
            <h2 class="top-h2">It's simple.</h2>
            <div> <img class="easy" src="https://lernable.com/wp-content/uploads/2017/06/easy.png" alt="onetwothree"> </div>
            <div class="steps-container"> <img class="one" src="https://lernable.com/wp-content/uploads/2017/06/one.png" alt="one">
                <div class="text-p">
                    <h2>Find something you want to learn.</h2>
                    <p>Go to lernable.com and scan every listing.
                        <br> Can’t find it? Let us know what you want to learn,
                        <br> and we’ll scour the earth for someone who will get you there.</p>
                </div> <img class="first-illo" src="https://lernable.com/wp-content/uploads/2017/09/one-illo.png" alt="first illustrtion"> </div>
            <div class="steps-container"> <img class="second-illo" src="https://lernable.com/wp-content/uploads/2017/09/two.png" alt="second illustrtaion"> <img class="two" src="https://lernable.com/wp-content/uploads/2017/09/two1.png" alt="two">
                <div class="text-p">
                    <h2>Book a lesson, and get learning.</h2>
                    <p>Book 1 or 100. Learn how to code today,
                        <br> learn to make your own furniture tomorrow
                        <br> and draw in virtual reality the day after. </p>
                </div> <img class="second-illo mobile" src="https://lernable.com/wp-content/uploads/2017/09/two.png" alt="second illustrtaion"> 
            </div>
            <div class="steps-container"> <img class="three" src="https://lernable.com/wp-content/uploads/2017/09/three.png" alt="three">
                <div id="show-up" class="text-p">
                    <h2>Show up, meet a future you.</h2>
                    <p>It may not seem like it to you but just a short time ago,
                        <br> our mentors set out to learn the same thing as you are now.
                        <br> So switch on, because with their help you can learn what they learnt
                        <br> in a fraction of the time, and with all the fun doing it. </p>
                </div> <img class="third-illo" src="https://lernable.com/wp-content/uploads/2017/09/third-illo.png" alt="third illustrtaion"> </div>
            <?php while ( have_posts() ) : the_post();

				do_action( 'storefront_page_before' );

				get_template_part( 'content', 'page' );

				/**
				 * Functions hooked in to storefront_page_after action
				 *
				 * @hooked storefront_display_comments - 10
				 */
			
            endwhile; // End of the loop. ?>
        </main>
    </div>
    <div class="home-hero howitworks2">
        <h2>Go to sleep, wake up, and do it all over again.</h2>
        <p>Some skills may be extremely fun and exciting at first,
            <br> but remember real skills takes time. So once you know where
            <br> and what buttons to push or when and where to go,
            <br> crunch time starts AFTER you leave the lesson. </p>
        <div class="call-to-action"> <a href="https://www.lernable.com" class="hero-description">see available lessons</a> </div>
    </div>
<div class="margin">
    <h2 id="faq">FAQs</h2>
    <div class="left-faq">
        <p class="subheading"> 1. How do you determine the price of a lesson?</p>

<p>The price of each lesson is determined by the mentor, the only thing we do is help them research how best to price themselves. Generally, prices are cheaper than the market because of the fact that mentors don’t have to pay rental costs for a space. 
        </p><p class="subheading">

        2. How much do mentors get paid?</p><p>

mentors get all proceeds barring an 11% fee.
        </p><p class="subheading">

        3. How do I know I can trust my mentor? </p><p>

        We personally know every single one of our mentors and each mentor runs through a process of vetting where we meet them face-to-face, just like you will. We run a practice lesson with them to ensure the quality of the experience, and have personally witnessed how amazing they are. </p><p class="subheading">


        4. What happens if I get injured during the experience?</p><p>

Unfortunately, at this stage of the company, we can’t afford insurance for each experience, and so you will have to accept any risks involved with the activities during the lesson.  

        </p><p class="subheading">
5. What happens if I change my mind about going to an experience? What if something important has come up and I just can’t make it?
        </p><p>
Our cancellation policy is that you will receive full refunds if you cancel in the next 24 hours after booking. If you can’t make it, as long as you notify the mentor a week before the lesson, you will be able to reschedule the lesson for another time more convenient for you.
        </p><p class="subheading">

        6. What if my mentor doesn’t turn up? </p><p>

We’re so sorry! In the unlikely event that your mentor doesn’t turn up to your lesson, we will provide a full refund. We will then take any necessary action with the mentor to ensure that it won’t happen again. 

        </p><p class="subheading">
        7. What if my experience was unsatisfactory?</p><p>

        If you feel as though you didn’t receive the quality of experience you expected, please let us know why straight away at <a href="mailto:contact@lernable.com?Subject=Not%20satisfied%20with%20experience" target="_blank">contact@lernable.com</a> and we will review the problem as soon as possible.
        </p>
<h2>
    Mentors</h2>
<p class="subheading">
    1. Who can mentor a lesson?</p ><p>

        To mentor a lesson you should meet the following standards:</p>
        <ul>
            <li>You’re passionate and deeply knowledgeable about your subject or skills at your activity and can demonstrate this to your Learners.</li><li>
	 
            You are genuine and love meeting people so that you can give your Learners the best possible outcome in the development of their skill</li>

            <li>You can anticipate your Learners needs and feelings. Everyone learns differently. To be an effective mentor you must be able to facilitate conversion to understand your Learners goals and feelings and be able to adapt the lesson accordingly.</li>

            <li>You must be over the age of 18.</li>

        </ul>

    </div>
    <div class="right-faq">
        <p class="subheading">
            2. How do I become a mentor?</p>
        <ol>
            <li>Fill out <a href="https://goo.gl/forms/ID8A0nLenRvfv8c42" target="_blank">an application form</a></li>
            <li>	We will then review the application and contact you.</li>
            <li>	To ensure that mentors are of a high quality each applicant must conduct a practice lessons with a Lernable representative. After we contact you, choose a time to conduct the practice lesson. Photos will be taken at these lessons to be used in your listing if you are successful.</li>
            <li> 	If you’re successful your listing is then created and you’re ready to teach!</li>
        </ol>
        <p class="subheading">3. What should I prepare for the practice lesson?</p>

        <p>A practice lesson should be set up exactly how you would teach a real lesson. A Lernable representative will come and experience the lesson and provide feedback and tips on how to improve the lesson. Photos will also be taken during the lesson so be ready to look and feel your best!</p>

        <p class="subheading">4. How do I structure my lessons?</p>

        <p>We can give you guidance on how to structure a lesson. You can also request a lesson guide after you fill out the application. The structure of the lesson will be mostly up to you as each skill is unique and each Learner is also unique.</p>


        <h2>Learners</h2>

        <p class="subheading">1. How do I become a Learner?</p>
<p>
    Use the Lernable site on your computer, phone or tablet to book an experience. Look for categories then choose a specific lesson or browse through all the lesson. </p>

<p>Availability for the lessons are set by the individual mentor.<p>

        <p>Before you book make sure to review the guest requirements, location, what to bring, age limits and skill level. </p>

        <p class="subheading">2. How do I find the booking confirmation for my upcoming lesson?</p>

        <p>You will receive a confirmation email for each lesson you book. This will go to the email address submitted at checkout.</p>


        <h2>Account</h2>

        <p class="subheading">1. Why can’t I create an account or log in?</p>

        <p>Our site currently does not allow you to log in as we’re still brand new and creating accounts can be annoying. In the near future you will be able to log in, access all your previous lessons, leave a review and create a profile.</p>


        <h2>Reviews</h2>

        <p class="subheading">1. How do I leave a review?</p>

        <p>Learners will be sent an email after the lesson has been completed to prompt you to leave a review. </p>




    </div>
    <div class="more-questions">
        <div class="break"><hr><hr><hr></div>
       
        <form class="form" action="https://lernable.com/how-it-works-contact" method="post"
              <?php 
if(isset($_GET[ 'contacted'])) { echo 'style="display:none;"'; }
?>
              >
             <h2 id="more-questions">Got more questions? <br> Get in touch.</h2>
														<div class="name">
															<label for="lernable_name">Name</label><br>
															<input type="text" name="lernable_name" class="nameinput input">
														</div>
														<div class="email">
															<label for="lernable_email">Email</label><br>
															<input type="text" name="lernable_email" class="emailinput input">
														</div>
														<div class="message">
															<label for="lernable_message">Message</label><br>
															<textarea name="lernable_message" class="messageinput input"></textarea>
														</div>
		
													   <input type="hidden" name="lernable_permalink" value="<?php echo get_permalink(); ?>">
														<div class="sendbtn"><input type="submit" name="lernable_submit" value="Send"></div>
													</form>
        <a name="thanks"></a>
        <div id="thanks" class="modal" 
<?php 
if(isset($_GET[ 'contacted'])) { echo 'style="display:block;"'; }
?>

>
											<!-- Modal content -->
						
												
									
												
														
													
															<h1 class="thanks-title">Thanks for contacting us!</h1>
															<p class="getback">We'll get back to you real soon.</p>
													
												
													
										
									
<!--										</div>-->
    </div>
    
</div>
    <?php
get_footer();