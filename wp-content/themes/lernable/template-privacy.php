<?php
/**
 * The template for displaying the privacy page.
 *
 * Template Name: Privacy
 *
 * @package lernable
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

            <?php while ( have_posts() ) : the_post();

				do_action( 'storefront_page_before' );

				get_template_part( 'content', 'page' );

				/**
				 * Functions hooked in to storefront_page_after action
				 *
				 * @hooked storefront_display_comments - 10
				 */
			?>
            <div class="tandp-container">
                <h1 class="lern-title">Privacy Policy</h1>
                <ol class="num">
                    <li>Introduction</li>
                    <ul class="dash">
                        <li>Lernable Co. ABN 54 510 149 855 ("Lernable" or "We") manages the information that We collect from you in accordance with all applicable privacy legislation in Australia.</li>
                        <li>This Privacy Policy describes how Lernable handles your personal information, and sets out the rights and obligations that both you and Lernable have in relation to your personal information.</li>
                        <li>By accessing www.lernable.com (the "Site") you accept and agree to the terms and conditions of Lernable's user agreement ("User Agreement"), which includes your consent to, and authorization of, the collection, use and disclosure of your personal information in accordance with this Privacy Policy.</li>
                        <li>If you see an undefined term in this Privacy Policy (such as &ldquo;Listing&rdquo; or &ldquo;Platform&rdquo;), it has the same definition as outlined in our <a href="http://lernable.com/index.php/terms/">Terms and Conditions</a>.</li>
                    </ul>



                    <li>Collection and Use of Your Personal Information</li>

                    <ul class="dash">
                        <li>We collect information you share with us when you use the Lernable Platform. A failure by you to provide information requested by us may mean that We are unable to provide some or all of our services to you.</li>
                        <ol class="a3">
                            <li>Profile information</li>
                            <p class="nodash4">Lernable collects personal information when you register with Lernable. This may include, but is not limited to, your name, address, phone number, contact details, birth date, gender, credit card and account details, occupation and interests. We might also give you the option of providing a photo or video to be associated with your Lernable user ID. Hosts must provide a photo or video to be associated with your Lernable user ID. If your personal details change, it is your responsibility to update your Lernable account with those changes, so that We can keep our records complete, accurate and up to date.</p>
                            <li>Authentication-Related Information</li>
                            <p class="nodash4">To help create and maintain a trusted environment, we may collect identification (like a photo of your government-issued ID) or other authentication information.</p>
                            <li>Payment Information</li>
                            <p class="nodash4">We collect your financial information (like your bank account or credit card information) when you use Site to process payments.</p>
                            <li>Other information</li>
                            <p class="nodash4">You may otherwise choose to provide us information when you fill in a form, contact us, conduct a search, update or add information to your Lernable Account, respond to surveys, post to community forums, participate in promotions, or use other features of the Lernable Platform. In addition to data collected from your submissions, we may also collect data on your internet behaviour from Facebook, other social media sites, and other services. If you use a location- enabled Lernable service, We may collect and process information about your actual location (for example, GPS signals sent by your mobile device). We may also use a range of different technologies to confirm your location.</p>
                        </ol>
                        <li>Lernable will use the information collected by it to protect Lernable and the users of the Site, to customer the content, to provide, maintain, protect and improve our services, to conduct research, to expand our user base, to develop our relationships with affiliate service providers and to generate data reports on an aggregated, non-personally identifiable basis for internal and third party use, but subject to applicable laws.</li>
                    </ul>

                    <li>Sharing and disclosure</li>
                    <ul class="dash">
                        <li>We may share your information at your direction or as described at the time of sharing, such as when you authorize a third party application or website to access your Lernable Account.</li>
                        <li>To help facilitate bookings, we may share your information with other Users when a Booking is made including your full name, your cancellation history, and other information you agree to share. When your booking is confirmed, we will disclose additional information to assist with coordinating the lesson, like your phone number.</li>
                        <li>The Lernable Platform lets you publish information that is visible to the general public such as parts of public profile page, your first name, your description, and city, are publicly visible to others.</li>
                        <li>Listings are publicly visible and include information such as the Listing's approximate location, Listing description, calendar availability, the Host's public profile, recently booked Listings and any additional information the Host chooses to share.</li>
                        <li>After completing a booking, Learners and Hosts may write reviews and rate each other. Reviews are part of your public profile page.</li>
                        <li>Information you share publicly on the Lernable Platform may be indexed through third party search engines.</li>
                        <li>Lernable may share your personal information to provide the payment service.</li>
                    </ul>

                    <li>Cookies</li>
                    <ul class="dash">
                        <li>If you have registered on the Site then your computer or device may store an identifying cookie or anonymous identifier, which can save you time each time you re-visit the Site, by accessing your information when you sign-in to Lernable.</li>
                        <li>Lernable may use cookies and anonymous identifiers for a number of purposes including to access your information when you sign in, keep track of your preferences, direct specific content to you, report on Lernable's user base, and to improve Lernable's services. We may also use cookies or anonymous identifiers when you interact with our affiliate service providers (for example, when you integrate your Lernable account with your Facebook profile).</li>
                    </ul>

                    <li>Protecting and maintaining personal information</li>
                    <ul class="dash">
                        <li>Your account is protected by a password for your privacy and security. We will take all reasonable steps to protect the information We hold about you from unauthorized access, use and disclosure, however We cannot guarantee the absolute security of that information, or that our systems will be completely free from third party interception or are incorruptible from viruses. We cannot and do not guarantee that information you send from your computer to us over the Internet will be protected by any form of encryption (encoding software). In light of this, We cannot and do not ensure or warrant the security or privacy of your personal information, including payment and account details. You transmit your personal information to us at your own risk.</li>
                        <li>You are entirely responsible for maintaining the security of your passwords and/or account information.</li>
                    </ul>

                    <li>Third Parties</li>
                    <ul class="dash">
                        <li>The Site may contain links to third party websites including the networks of our valued affiliate service providers, advertisers, and PayPal, or make available services obtained from third parties, including verification services by third party verification providers. If you follow a link to any of these websites, for instance PayPal payment system, or use any services obtained from third party service providers via the Site that requires you to provide personal information (including sensitive information if relevant) directly to such third parties (for instance third party verification providers), note that they have their own privacy policies. If you use our Site to link to another site, or use a service obtained from a third party service provider via the Site, you will be subject to that site's or third party's terms and conditions of use, privacy policy and security statement. We strongly encourage you to view these before disclosing any of your personal information on such sites.</li>
                        <li>Third-party vendors, including Google, may show Lernable ads on sites across the Internet.</li>
                    </ul>

                    <li>Marketing</li>
                    <ul class="dash">
                        <li>Lernable may contact you as the result of a referral by another user of the Site who has provided us with contact information, such as your name and email address or if you have opted in to receive updates about our services. The use of contact information received in connection with a referral will be governed by this Privacy Policy. You may, at any time, opt-out of Lernable's referral system by emailing Lernable using the contact information provided on the Site.</li>
                    </ul>

                    <li>Contact</li>
                    <ul class="dash">
                        <li>If you have any questions or complaints about this Privacy Policy or Lernables&rsquo;s information handling practices, you may contact us at <a href="mailto:contact@lernable.com">contact@lernable.com</a></li>
                    </ul>
                </ol>

            </div>

            <?php
				do_action( 'storefront_page_after' );

			endwhile; // End of the loop. ?>

        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->

    <?php
get_footer();