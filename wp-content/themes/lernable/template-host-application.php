<?php
/**
 * The template for displaying the host application page.
 *
 * Template Name: Host Application
 *
 * @package lernable
 */

get_header(); ?>

    <?php 
if(isset($_POST['submit'])){
    $to = "contact@lernable.com"; // this is your Email address
    $from = $_POST['email']; // this is the sender's Email address
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $subject = "Form submission";
    $subject2 = "Copy of your form submission";
    $message = $first_name . " " . $last_name . " wrote the following:" . "\n\n" . $_POST['message'];
    $message2 = "Here is a copy of your message " . $first_name . "\n\n" . $_POST['message'];

    $headers = "From:" . $from;
    $headers2 = "From:" . $to;
    mail($to,$subject,$message,$headers);
    mail($from,$subject2,$message2,$headers2); // sends a copy of the message to the sender
    echo "Mail Sent. Thank you " . $first_name . ", we will contact you shortly.";
    // Should redirect to another page to avoid resubmission
    // You can also use header('Location: thank_you.php'); to redirect to another page.
    }
?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

            <h2>Apply to become a mentor</h2>
            <p>(Emails contact@lernable.com)</p>

            <h2>Tell us about YOU!</h2>

            <form action="" method="post">
                
                <label for="first_name">First Name:</label><input type="text" name="first_name" placeholder="Jane"><br>
                <label for="last_name">Last Name:</label><input type="text" name="last_name" placeholder="Doe"> <br> 
                <label>Birthday:</label>
                <select name="DOBDay">
	<option> - Day - </option>
	<option value="1">1</option>
	<option value="2">2</option>
	<option value="3">3</option>
	<option value="4">4</option>
	<option value="5">5</option>
	<option value="6">6</option>
	<option value="7">7</option>
	<option value="8">8</option>
	<option value="9">9</option>
	<option value="10">10</option>
	<option value="11">11</option>
	<option value="12">12</option>
	<option value="13">13</option>
	<option value="14">14</option>
	<option value="15">15</option>
	<option value="16">16</option>
	<option value="17">17</option>
	<option value="18">18</option>
	<option value="19">19</option>
	<option value="20">20</option>
	<option value="21">21</option>
	<option value="22">22</option>
	<option value="23">23</option>
	<option value="24">24</option>
	<option value="25">25</option>
	<option value="26">26</option>
	<option value="27">27</option>
	<option value="28">28</option>
	<option value="29">29</option>
	<option value="30">30</option>
	<option value="31">31</option>
</select>

                <select name="DOBMonth">
	<option> - Month - </option>
	<option value="January">January</option>
	<option value="Febuary">Febuary</option>
	<option value="March">March</option>
	<option value="April">April</option>
	<option value="May">May</option>
	<option value="June">June</option>
	<option value="July">July</option>
	<option value="August">August</option>
	<option value="September">September</option>
	<option value="October">October</option>
	<option value="November">November</option>
	<option value="December">December</option>
</select>

    <select name="DOBYear">
	<option> - Year - </option>

 	<option value="1993">2002</option>
	<option value="1992">2001</option>
	<option value="1991">2000</option>
	<option value="1990">1999</option>
	<option value="1989">1998</option>
	<option value="1988">1997</option>
	<option value="1987">1996</option>
	<option value="1986">1995</option>
	<option value="1985">1994</option>                   
	<option value="1993">1993</option>
	<option value="1992">1992</option>
	<option value="1991">1991</option>
	<option value="1990">1990</option>
	<option value="1989">1989</option>
	<option value="1988">1988</option>
	<option value="1987">1987</option>
	<option value="1986">1986</option>
	<option value="1985">1985</option>
	<option value="1984">1984</option>
	<option value="1983">1983</option>
	<option value="1982">1982</option>
	<option value="1981">1981</option>
	<option value="1980">1980</option>
	<option value="1979">1979</option>
	<option value="1978">1978</option>
	<option value="1977">1977</option>
	<option value="1976">1976</option>
	<option value="1975">1975</option>
	<option value="1974">1974</option>
	<option value="1973">1973</option>
	<option value="1972">1972</option>
	<option value="1971">1971</option>
	<option value="1970">1970</option>
	<option value="1969">1969</option>
	<option value="1968">1968</option>
	<option value="1967">1967</option>
	<option value="1966">1966</option>
	<option value="1965">1965</option>
	<option value="1964">1964</option>
	<option value="1963">1963</option>
	<option value="1962">1962</option>
	<option value="1961">1961</option>
	<option value="1960">1960</option>
	<option value="1959">1959</option>
	<option value="1958">1958</option>
	<option value="1957">1957</option>
	<option value="1956">1956</option>
	<option value="1955">1955</option>
	<option value="1954">1954</option>
	<option value="1953">1953</option>
	<option value="1952">1952</option>
	<option value="1951">1951</option>
	<option value="1950">1950</option>
	<option value="1949">1949</option>
	<option value="1948">1948</option>
	<option value="1947">1947</option>
	
</select><br> 
                
                <label for="email">Email:</label><input type="text" name="email" placeholder="someone@example.com"><br>
                <label for="mobile">Mobile:</label> <input type="text" name="mobile" placeholder="0411 111 111"><br>
                Street, suburb, state: <input type="text" name="address" placeholder="Example Rd, Enmore, New South Wales"><br>
                General experience related to skill(s): <input type="text" name="general_experience" placeholder="E.g. I have been playing the harp for 12 years"><br>
                Teaching Experience: <input type="text" name="teaching_experience" placeholder="E.g. Started teaching with lernable in May 2017"><br>
                Awards and professional affiliations: <input type="text" name="awards" placeholder="E.g. Masters in Mechanical Engineering UNSW"><br>
                Languages spoken: <input type="text" name="languages" placeholder="E.g. Native in Chinese, fluent in English"><br>

                <h2>Listing Details</h2>
                <h3>Example listing page (INSERT IMAGE HERE)</h3>
                
                Title of Lesson: <p>Try to make this weird and quirky, not just like any old lesson. E.g. 'Chinese Harp and Dumplings' as opposed to 'Learn the Harp'</p><input type="text" name="title" placeholder="E.g. Chinese Harp and Dumplings"><br>
                Tagline: <p>This will appear next to the title and will be the first thing a customer reads on your listing. It's important for this to be catchy and interesting.</p><input type="text" name="tagline" placeholder="E.g. Learn to play the Chinese Harp (Guzheng) and eat delicious dumplings"><br>
                About me: <p>What makes you qualified to be a teacher of this skill? What makes you interesting? Again, think creatively.</p><input type="text" name="about_me" placeholder="E.g. In the last 10 years that I have played the harp, I have met Ice Cube and played for the Prime Minister. Pretty cool, hey?"><br>
                
                Category:
                  <input type="radio" name="category" value="music" checked> Music<br>
  <input type="radio" name="category" value="art"> Art<br>
  <input type="radio" name="category" value="other"> Other <input type="text" name="other_category" placeholder="A category that doesn't exist yet"><br>
                <!--  Use something like this at the top
$phone = mysql_real_escape_string($_POST['phone']);
if ($phone =='other'){
  $phone = mysql_real_escape_string($_POST['phone-other']);
}
-->
                Customer skill level: <input type="text" name="customer_skill" placeholder="E.g. Never played, beginner"><br>
                Select types of lessons and lesson packages you will offer:
                <input type="checkbox" name="lesson_type" value="single">Single lessons<br>
<input type="checkbox" name="lesson-type" value="5course">5 Lesson Course<br>
                <input type="checkbox" name="lesson_type" value="other">Other: <input type="text" name="other_lesson_type" placeholder="A different lesson type"><br>
                
                Group size of lesson: <input type="text" name="group_size" placeholder="E.g. Max 1 person"><br>
                Duration of lesson: <input type="text" name="duration" placeholder="E.g. 45 mins per lesson"><br>
                Price per lesson: <input type="text" name="price" placeholder="Range or single price"><br>
                What the customer will learn: <input type="text" name="learn" placeholder="E.g. Basic plucking technique, how to play a simple version of a popular song, the lesson will be customised according to the learners"><br>
                What's included: <input type="text" name="included" placeholder="E.g. Harp, Dumplings, Chopsticks"><br>
                What to bring: <input type="text" name="bring" placeholder="ID that matches account details for proof of identity (required by Lernable) plus anything else the customer should bring"><br>
                Comments: <input type="text" name="comments" placeholder="Any extra comments"><br>
                <input type="submit" name="submit" value="Submit">
            </form>

        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->

    <?php
get_footer();