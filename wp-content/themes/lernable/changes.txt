// This is a text file which describes every change made outside of adding to the lernable folder (e.g. changing a woocommerce function that is declared before functions.php)

1. Commented out wc_create_new_customer function in wc-user-functions.php (overwritten in functions.php)
2. Commented out woocommerce_get_product_thumbnail in wc-template-functions.php (overwritten in functions.php)
3. Commented out and replaced code for styling of credit card input iframes (in checkout) in function get_hosted_field_styles() in woocommerce-gateway-paypal-powered-by-braintree/includes/payment-forms/class-wc-braintree-hosted-fields-payment-form.php
4. Commented out and replace line in wp-comments-post.php ~ line 48 to redirect reviews to correct location
5. Added code to wp-comments-post.php ~ line 50 to send email to lernable and to host with review information.