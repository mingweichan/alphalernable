<?php
/**
 * The template for displaying all pages.
 *
 * Template Name: Thank You
 *
 * @package lernable
 */

get_header(); 

$post_id = $_GET["id"];
$product = wc_get_product($post_id);

$thankyou_image = $product->get_attribute( 'invoice_image' );

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="thankyou-container">
				<h1 class="thankyou-title">Thank you!</h1>
				<div class="thankyou-content">
					<p class="thankyou-text">Your feedback really helps us and your host improve the lesson experience.</p>
					<p class="thankyou-text">No, really. We're so thankful for your feedback that if you're up for it, we would love to buy you a coffee and get to know you!</p>
					<p class="thankyou-small">If you would like a free coffee at the expense of the Founders at Lernable, please write to us at <a href="mailto:contact@lernable.com">contact@lernable.com</a>.</p>
					<a href="https://lernable.com/" class="back-to-home">Back to home</a>
				</div>
				<div class="thankyou-image">
					<img class="thankyou-image" src="<?php echo $thankyou_image; ?>">
				</div>
			</div>





			<?php /*while ( have_posts() ) : the_post();

				do_action( 'storefront_page_before' );

				get_template_part( 'content', 'page' );

				/**
				 * Functions hooked in to storefront_page_after action
				 *
				 * @hooked storefront_display_comments - 10
				 *
				do_action( 'storefront_page_after' );

			endwhile; // End of the loop. */?>

		</main>
		<!-- #main -->
	</div>
	<!-- #primary -->

	<?php
do_action( 'storefront_sidebar' );
get_footer();