<!--THE FOLLOWING IS TO BE MODIFIED AND THEN COPIED AND PASTED INTO WORDPRESS CMS-->

<div class="people-duration">
    <div><i class="fa fa-user" aria-hidden="true"></i></div>
    <p class="next-to-icon">3 people max</p><br/>
    </div>
<div class="people-duration">
    <div><i class="fa fa-clock-o" aria-hidden="true"></i> </div>
    <p class="next-to-icon">2 hours/lesson</p>

</div>
<div class="single-listing-details">
    <p class="font-reg">Your skill level</p>
    <p>Beginner, possibly never tried VR</p>
    <p class="font-reg">What you'll learn</p>
    <p>- what the different VR technologies are</p>
    <p>- try on different headsets</p>
    <p>- play some VR games</p>
    <p>- intro to Tiltbrush, Quill and Medium</p>
    <p>- understanding how to bring in models</p>
    <p>- how to create an environment and to draw from reference images</p>
    <p>- how to export, record a video and share drawings</p>
    <p class="font-reg">What's included</p>
    <p>- VR headsets: Oculus Rift, GearVR and cardboard</p>
    <p>- Heaps of games and apps</p>
    <p>- 3D models</p>
    <p class="font-reg">Where</p>
    <p>Preston Pl, Roseville</p>
    <div class="map-img"><img src="http://lernable.com/wp-content/uploads/2017/07/vrmap-300x191.png" alt="" class="alignnone size-medium wp-image-106" />
    </div>
    <p class="font-reg">What to bring</p>
    <p>ID that matches account details for proof of identity</p>
    <p>A picture of what you want to draw in VR</p>
    <p class="font-reg">Refund policy</p>
    <p>If you would like to cancel within 24 hours of booking, a full refund will be issued</p>
    
</div>