<?php
/**
 * The template for displaying the terms and conditions page.
 *
 * Template Name: Add Review
 *
 * @package lernable
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="add-review-page">

				<?php //while ( have_posts() ) : the_post();

				//do_action( 'storefront_page_before' );
				
				$post_id = $_GET["id"];
				$product = wc_get_product($post_id);

				$review_image = $product->get_attribute( 'completed_image' );
          
          ?>

				<h1 class="lern-title">Leave a review for
					<?php echo get_the_title($_GET["id"]); ?>
				</h1>
				
				<?php if ( get_option( 'woocommerce_review_rating_verification_required' ) === 'no' || wc_customer_bought_product( '', get_current_user_id(), $product->get_id() ) ) :/* ?>


				<div id="review_form_wrapper">
					<div id="review_form">
						<form action="https://lernable.com/wp-comments-post.php" method="post" id="commentform" class="comment-form" novalidate _lpchecked="1">
							<div id="respond">
								<p class="comment-form-author"><label for="author">Name</label> <input id="author" name="author" type="text" value="" ria-required="true" required=""></p>

								<p class="comment-form-email"><label for="email">Email</label> <input id="email" name="email" type="email" value="" aria-required="true" required=""></p>

								<div class="comment-form-rating">
									<select name="rating" id="rating" aria-required="true" required="" style="display: none;">
									<option value="">Rate…</option>
									<option value="5">Perfect</option>
									<option value="4">Good</option>
									<option value="3">Average</option>
									<option value="2">Not that bad</option>
									<option value="1">Very poor</option>
								</select>
								</div>
								<p class="comment-form-comment">
									<label for="comment">Write a little bit about your experience. This will appear below the mentor's listing to help other learners understand the quality of the experience!</label>
									<textarea id="comment" name="comment" aria-required="true" required=""></textarea>
								</p>
								<button id="review-next" class="review-next">Next</button>
								<p class="form-submit">
									<input name="submit" type="submit" id="submit" class="submit" value="Submit">
									<input type="hidden" name="comment_post_ID" value="<?php echo $_GET[" id "]; ?>" id="comment_post_ID">
								</p>
							</div>
						</form>
						<?php */
					$comment_form = array(
						'fields'               => array(
							'author' => '<p class="comment-form-author">' . '<label for="author">Name</label> ' .
										'<input id="author" name="author" type="text" value="" aria-required="true" required /></p>',
							'email'  => '<p class="comment-form-email"><label for="email">Email</label> ' .
										'<input id="email" name="email" type="email" value="" aria-required="true" required /></p>',
						),
						'label_submit'  => __( 'Submit', 'woocommerce' ),
						'logged_in_as'  => '',
						'comment_field' => '',
						'title_reply' => '',
						'comment_notes_after' => 
	'<span id="review-next">Next</span>' .
		'<div id="private-feedback-page" style="display:none;">' .
			'<h2 class="review-section-title">Anything to say privately to your mentor?</h2>' .
			'<p class="review-section-text">Which of these could have been better? Any feedback is incredibly important to your mentor in order for them to refine their lesson experience.</p>' .
			'<div class="improvement-options">' .
				'<input type="checkbox" style="display:none" name="improve[]" id="communication" value="Communication"><label for="communication"><div class="improvement-buttons first">Communication</div></label>' .
				'<input type="checkbox" style="display:none" name="improve[]" id="timeliness" value="Timeliness"><label for="timeliness"><div class="improvement-buttons">Timeliness</div></label>' .
				'<input type="checkbox" style="display:none" name="improve[]" id="knowledge" value="Knowledge"><label for="knowledge"><div class="improvement-buttons">Knowledge</div></label>' .
				'<input type="checkbox" style="display:none" name="improve[]" id="environment" value="Environment"><label for="environment"><div class="improvement-buttons">Environment</div></label>' .
				'<input type="checkbox" style="display:none" name="improve[]" id="equipment" value="Equipment"><label for="equipment"><div class="improvement-buttons last">Equipment</div></label>' .
			'</div>' .
			'<label class="improvement-text-label" for="improvement-text">Tell us why:</label><textarea id="improvement-text" name="improvement-text"></textarea>' .
			'<span class="optional-feedback-button">Leave feedback for us at Lernable</span>' .
		'</div>' .
		'<div id="lernable-feedback-page" style="display:none;">'	.
			'<h2 class="review-section-title">Is there anything you\'d like to share with Lernable?</h2>' .
			'<p class="review-section-text">This won\'t be shared with your mentor. Also please leave any feedback or feature suggestions about the website! We are constantly looking for ways to improve our online experience.' .
			'<textarea name="lernable-feedback-text" id="lernable-feedback-text"></textarea>' .
		'</div>',	
					);

					if ( $account_page_url = wc_get_page_permalink( 'myaccount' ) ) {
						$comment_form['must_log_in'] = '<p class="must-log-in">' . sprintf( __( 'You must be <a href="%s">logged in</a> to post a review.', 'woocommerce' ), esc_url( $account_page_url ) ) . '</p>';
					}

					if ( get_option( 'woocommerce_enable_review_rating' ) === 'yes' ) {
						$comment_form['comment_field'] = '<div class="comment-form-rating"><select name="rating" id="rating" aria-required="true" required>
							<option value="">' . esc_html__( 'Rate&hellip;', 'woocommerce' ) . '</option>
							<option value="5">' . esc_html__( 'Perfect', 'woocommerce' ) . '</option>
							<option value="4">' . esc_html__( 'Good', 'woocommerce' ) . '</option>
							<option value="3">' . esc_html__( 'Average', 'woocommerce' ) . '</option>
							<option value="2">' . esc_html__( 'Not that bad', 'woocommerce' ) . '</option>
							<option value="1">' . esc_html__( 'Very poor', 'woocommerce' ) . '</option>
						</select></div>';
					}

					$comment_form['comment_field'] .= '<p class="comment-form-comment"><label for="comment">Write a little bit about your experience. This will appear below the mentor\'s listing to help other learners understand the quality of the experience!</label><textarea id="comment" name="comment" aria-required="true" required></textarea></p>';

					comment_form( apply_filters( 'woocommerce_product_review_comment_form_args', $comment_form ) , $_GET["id"] );	
				?>
				
				<div class="review-image"><img class="review-image" src="<?php echo $review_image; ?>"></div>

				<?php else : ?>

				<p class="woocommerce-verification-required">
					<?php _e( 'Only logged in customers who have purchased this product may leave a review.', 'woocommerce' ); ?>
				</p>

				<?php endif; ?>

				<?php
				do_action( 'storefront_page_after' );

			//endwhile; // End of the loop. ?>
			</div>
		</main>
		<!-- #main -->
	</div>
	<!-- #primary -->


	<?php get_footer(); ?>

	<script type='text/javascript'>
		/* <![CDATA[ */
		var wc_single_product_params = {
			"i18n_required_rating_text": "Please select a rating",
			"review_rating_required": "yes",
			"flexslider": {
				"rtl": false,
				"animation": "slide",
				"smoothHeight": false,
				"directionNav": false,
				"controlNav": "thumbnails",
				"slideshow": false,
				"animationSpeed": 500,
				"animationLoop": false
			},
			"zoom_enabled": "",
			"photoswipe_enabled": "1",
			"flexslider_enabled": "1"
		};
		/* ]]> */
	</script>
	<script type='text/javascript' src='//lernable.com/wp-content/plugins/woocommerce/assets/js/frontend/single-product.min.js?ver=3.0.8'></script>
	<script type='text/javascript' src='//lernable.com/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script>
	<script type='text/javascript' src='//lernable.com/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4'></script>
	<script type="text/javascript" src="//lernable.com/wp-content/themes/lernable/js/add-review.js"></script>

