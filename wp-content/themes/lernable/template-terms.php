<?php
/**
 * The template for displaying the terms and conditions page.
 *
 * Template Name: Terms
 *
 * @package lernable
 */

get_header(); ?>
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <?php while ( have_posts() ) : the_post();

				do_action( 'storefront_page_before' );

				get_template_part( 'content', 'page' );

				/**
				 * Functions hooked in to storefront_page_after action
				 *
				 * @hooked storefront_display_comments - 10
				 */
			?>
                <div class="tandp-container">
                    <h1 class="lern-title">Terms and Conditions</h1>
                    <ol class="num">
                        <li>Key Terms</li>
                        <ul class="keyterms">
                            <li>&ldquo;ACL&rdquo; means Australian Consumer Law.</li>
                            <li>&ldquo;Agreement&rdquo; means the most updated version of the agreement between Lernable and a User.</li>
                            <li>&ldquo;Booking&rdquo; means the purchasing of the services described in a listing on the Site.</li>
                            <li>&ldquo;Content&rdquo; means text, graphics, images, music, software, audio, video, information and other materials.</li>
                            <li>&ldquo;Lernable&rdquo; &ldquo;we&rdquo; &ldquo;us&rdquo; &ldquo;our&rdquo; means Lernable Co. ABN 54 510 149 855.</li>
                            <li>&ldquo;Mentor&rdquo; means a User who creates a Listing via the Site and Services.</li>
                            <li>&ldquo;Learner&rdquo; means a User who makes a Booking via the Site and Services.</li>
                            <li>&ldquo;Listing&rdquo; means a lesson or course that is listed by a Mentor as available for Booking via the Site and Services.</li>
                            <li>&ldquo;Services&rdquo; means the ability to create a listing or make a booking.</li>
                            <li>&ldquo;Site&rdquo; means https://www.lernable.com</li>
                            <li>&ldquo;User&rdquo; means a person who completed Lernable&rsquo;s account registration process.</li>
                        </ul>
                        <li>Introduction</li>
                        <ul class="dash">
                            <li>By using the site, you agree to comply with the full Terms and Conditions (the “Agreement”) when accessing or using our Services.</li>
                            <li>Lernable.com (the “Website”), is an online platform operating by Lernable through which Mentors may create listings for services and Learners may book lessons (and possibly workshops) directly with the Mentors.</li>
                            <li>You acknowledge and agree that, by accessing or using the site and or services, you are indicating that you have read, and that you understand and agree to be bound by these terms and conditions.</li>
                            <li>Lernable is not a party to any agreements entered between Mentors and Learners, nor is Lernable an agent or insurer. Lernable has no control over the conduct of the Mentors, Learners and other users of the site and services and disclaims all liability in regard to the maximum extent permitted by law.</li>
                            <li>Lernable reserves the right, at its sole discretion, to modify the Site or Services or to modify these Terms and Conditions, including the Service Fees, at any time and without prior notice.</li>
                        </ul>
                        <li>Eligibility</li>
                        <ul class="dash">
                            <li>You must be over 18 to access or use the Site and or Services. By accessing or using the Site and or Services you represent and warrant that you are over 18. Lessons may be booked for children under the age of 18 however a parent or guardian must be present at all times.</li>
                        </ul>
                        <li>Using the Site and Services</li>
                        <ul class="dash">
                            <li>Lernable provides a platform to enable Mentors to publish Listings and for Learners to make Bookings. The Site and platform is intended to be used to facilitate Mentors and Learners connecting and making Bookings for Services directly with each other. Lernable is not responsible for any disclaims any and all liability related to any and all Listings. Accordingly, any Bookings will be made at the User’s own risk.</li>
                        </ul>
                        <li>Account Registration</li>
                        <ul class="dash">
                            <li>In order to create a listing or make a booking you must register to create an account.</li>
                        </ul>
                        <li>Lesson Listings</li>
                        <ul class="dash">
                            <li>If you choose to create a listing, you understand and agree that your relationship with Lernable is limited to being a member and an independent, third-party contractor, not an employee, agent, joint venturer or partner of Lernable.</li>
                            <li>Listings will be made publicly available via the Site and Services.</li>
                            <li>Lernable reserves the right, at any time and without prior notice, to remove or disable access to any Listing for any reason, including Listings that Lernable, in its sole discretion, considers to be objectionable for any reason, in violation of these Terms.</li>
                            <li>In order to create a listing, a User must fill out the application form on the Site.</li>
                            <li>As a User you understand that not all applications will mean the successful creation of a listing. In order to successfully create a listing, as a User, you agree to undertake an interview with Lernable and any necessary identification verification procedures as deemed necessary by Lernable.</li>
                        </ul>
                        <li>No Endorsement</li>
                        <ul class="dash">
                            <li>Lernable does not endorse any User or Listing.</li>
                            <li>Any references in the Site or Services to a User being “verified” or “connected” (or similar language) only indicate that the User has completed a relevant verification or identification process, and does not represent anything else.</li>
                        </ul>
                        <li>Bookings and Financial Terms</li>
                        <ol class="a2">
                            <li>Booking and Financial Terms for Mentors</li>
                            <ul class="dash3">
                                <li>Each Mentor agrees that Lernable may, in accordance with the cancellation policy selected by the Mentor and reflected in the relevant Listing, (i) permit the Learner to cancel the Booking and (ii) refund (via Lernable) to the Learner the amount of the Booking.</li>
                                <li>Each Mentor agrees that Lernable will collect the total Booking amount from Learners at the time of the booking and hold the amount until after the service has been completed before paying the Mentor.</li>
                            </ul>
                            <li>Booking and Financial Terms for Learners</li>
                            <ul class="dash3">
                                <li>The Mentors, not Lernable, are solely responsible for honoring any confirmed Bookings and making available any Accommodations reserved through the Site.</li>
                                <li>You acknowledge and agree that you, and not Lernable, will be responsible for performing the obligations of any such agreements, that Lernable is not a party to such agreements.</li>
                            </ul>
                            <li>Service Fees</li>
                            <ul class="dash3">
                                <li>“Service fees” means 11% of the total Booking amount Lernable charges for the use of the services.</li>
                            </ul>
                            <li>Cancellations and Refunds</li>
                            <ul class="dash3">
                                <li>If, as a Learner, you wish to cancel a confirmed Booking made via the Site, either prior to the lesson, the cancellation policy of the Mentor contained in the applicable Listing will apply to such cancellation. Our ability to refund the Booking amount and other amounts charged to you will depend upon the terms of the applicable cancellation policy.</li>
                                <li>If a Mentor cancels a confirmed Booking made via the Site, Lernable will refund the total amount for such Booking.</li>
                            </ul>
                        </ol>
                        <li>Taxes</li>
                        <ul class="dash">
                            <li>Tax regulations may require us to collect appropriate tax information from our Mentors, or to withhold taxes from payouts to Mentors, or both.</li>
                        </ul>
                        <li>Damage to Mentor Property</li>
                        <ul class="dash">
                            <li>You acknowledge and agree that, as a Learner, you are responsible for your own acts and omissions and are also responsible for the acts and omissions of any individuals whom you invite to the lesson, subject to Mentor approval.</li>
                            <li>In the event that a Mentor claims otherwise and provides evidence of damage, including but not limited to photographs, you agree to pay the cost of replacing the damaged items with equivalent items.</li>
                        </ul>
                        <li>Privacy</li>
                        <ul class="dash">
                            <li>Lernable collects information you share with us when you use the Lernable platform.</li>
                            <li>We may only share your information with you consent or as described at the time of sharing, such as when you authorize a third party application or website to access your Lernable Account.</li>
                            <li>To help facilitate bookings, we may share your information with other Users such as when you are a Mentor and a Learner has made a Booking or when you are a Learner and you have made a Booking.</li>
                        </ul>
                        <li>Intellectual Property Ownership</li>
                        <ul class="dash">
                            <li>You acknowledge and agree that the Site, Services and Content, including all associated intellectual property rights, are the exclusive property of Lernable and its licensors.</li>
                        </ul>
                        <li>Lernable and Member Content</li>
                        <ul class="dash">
                            <li>By making available any User Content on or through the Site and Services, or through Lernable promotional campaigns, you hereby grant to Lernable a worldwide, irrevocable, perpetual (or for the term of the protection), non-exclusive, transferable, royalty-free license, with the right to sublicense, to use, view, copy, adapt, translate, modify, distribute, license, sell, transfer, publicly display, publicly perform, transmit, stream, broadcast, access, view, and otherwise exploit such User Content on, through, by means of or to promote or market the Site and Services.</li>
                        </ul>
                        <li>Limitation of Liability</li>
                        <ul class="dash">
                            <li>YOU ACKNOWLEDGE AND AGREE THAT, TO THE MAXIMUM EXTENT PERMITTED BY LAW, THE ENTIRE RISK ARISING OUT OF YOUR ACCESS TO AND USE OF THE SITE, APPLICATION, SERVICES AND COLLECTIVE CONTENT, YOUR LISTING OR BOOKING OF ANY LESSONS OR WORKSHOPS VIA THE SITE, APPLICATION AND SERVICES, YOUR PARTICIPATION IN THE REFERRAL PROGRAM, AND ANY CONTACT YOU HAVE WITH OTHER USERS OF LERNABLE WHETHER IN PERSON OR ONLINE REMAINS WITH YOU.</li>
                        </ul>
                        <li>Indemnification</li>
                        <ul class="dash">
                            <li>You agree to release, defend, indemnify, and hold Lernable and its affiliates and subsidiaries, and their officers, directors, employees and agents, harmless from and against any claims, liabilities, damages, losses, and expenses, including, without limitation, reasonable legal and accounting fees, arising out of or in any way connected with (a) your access to or use of the Site, Services, or Content or your violation of these Terms; (b) your User Content; (c) your (i) interaction with any User, (ii) Booking of a lesson, or (iii) creation of a Listing; (d) the use, condition or Booking of an lesson by you, including but not limited to any injuries, losses, or damages (compensatory, direct, incidental, consequential or otherwise) of any kind arising in connection with or as a result of a Booking.</li>
                        </ul>
                        <li>Links</li>
                        <ul class="dash">
                            <li>The Site and or Services may contain links to third-party websites or resources. You acknowledge that Lernable has no affiliation and responsibility over the content available from such websites or resources and you assume all risk arising from use of such websites.</li>
                        </ul>
                        <a name="learner-waiver"></a>
                    </ol>
                    
                    <a name="learner-waiver"></a>
                    <h1 class="lern-title">Learner Release and Waiver</h1>
                    <p class="learner-waiver-p">In order to participate in a Lesson, your Mentor(s) require(s) you to accept this Learner Release and Waiver, which is effective between you and your Mentor(s) as of the date when you first book or participate in a Lesson, whichever happens first. All terms not defined here have the meaning given them in the Lernable Terms and Conditions.</p>
                    <p class="learner-waiver-p"> You represent that you are 18 years of age or older. If you are bringing a minor as a Learner, you acknowledge and agree that you are solely responsible for the supervision of that minor throughout the duration of your Lesson, and have read this Learner Release and Waiver and agree to it on the minor’s behalf. If you are booking a Lesson on behalf of other Learners, you will ensure, and you represent and warrant, that each Learner on whose behalf you book has read and agreed to this Learner Release and Waiver, which shall apply to each of them as if the reference to “you” was a reference to him/her. </p>
                    <ol class="num">
                        <li>Assumption of Risks</li>
                        <ul class="dash">
                            <li>You understand and acknowledge that the Lessons(s) you sign up to do may be hazardous and may carry the risk of injury or illness, including sickness, physical injury, property damage, disability, permanent paralysis, and death. </li>
                            <li> TO THE MAXIMUM EXTENT PERMITTED UNDER APPLICABLE LAW, YOU KNOWINGLY, VOLUNTARILY AND FREELY ASSUME ALL RISKS, BOTH KNOWN AND UNKNOWN, OF PARTICIPATING IN EACH LESSON, EVEN IF THOSE RISKS ARISE FROM THE NEGLIGENCE OR CARELESSNESS OF THE MENTOR OR OTHERS, OR DEFECTS IN THE EQUIPMENT, PREMISES, OR FACILITIES USED DURING THE EXPERIENCE, OR OTHERWISE, AND YOU ASSUME FULL RESPONSIBILITY FOR PARTICIPATION IN THE EXPERIENCE. </li>
                        </ul>
                        <li>Release and Waiver</li>
                        <ul class="dash">
                            <li>You acknowledge and agree that: </li>
                            <ul>
                                <li>You have reasonably assessed the risks involved in the Lesson(s) and have made an informed and voluntary choice to participate. </li>
                                <li>You alone, and not your Mentor(s), are responsible for determining your fitness for participating in the Lesson(s) and your ability to fully understand any directions or warnings presented.</li>
                                <li> You will not participate in any Lesson(s) when you have a physical, medical, or mental limitation or disability, or when you are aware or should reasonably be aware of any factors that may limit or prevent you from safely participating in that Lesson(s). </li>
                                <li>You will act reasonably and responsibly and will comply with any provided and customary conditions, directions, and/or precautions for participation in the Lesson(s). If you notice any hazard during a Lesson, you will stop participating in the Lesson immediately.</li>
                            </ul>
                            <li>TO THE MAXIMUM EXTENT PERMITTED BY LAW, YOU RELEASE AND PROMISE NOT TO SUE YOUR MENTOR(S) FOR ANY CLAIMS, DEMANDS, CAUSES OF ACTION, LOSSES (WHETHER ECONOMIC OR NON-ECONOMIC), DAMAGES, EXPENSES, COSTS OR LIABILITY OF ANY NATURE WHATSOEVER ARISING FROM OR IN CONNECTION WITH YOUR LESSON(S), WHETHER BASED ON WARRANTY, CONTRACT, TORT (INCLUDING NEGLIGENCE), PRODUCT LIABILITY, OR ANY OTHER LEGAL THEORY. </li>
                            <li>You intend this Learner Release and Waiver to be a complete and unconditional release of all liability to the greatest extent allowed by law. You agree that if any portion of this Waiver and Release is held to be invalid, the balance notwithstanding shall continue in full force and effect. </li>
                        </ul>
                        <li>Disclaimer of Warranties</li>
                        <ul class="dash">
                            <li>TO THE MAXIMUM EXTENT PERMITTED BY LAW, MENTORS PROVIDE THE LESSON(S) “AS IS,” WITHOUT WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED. WITHOUT LIMITING THE FOREGOING AND TO THE MAXIMUM EXTENT PERMITTED BY LAW, MENTOR(S) EXPRESSLY DISCLAIM ANY WARRANTIES OF SAFETY, FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT, AND AS TO THE ADEQUACY OF THE DIRECTIONS AND WARNINGS PROVIDED TO YOU. </li>
                             <strong>Indemnification
                            </strong> 
                            <li>You agree that if, despite this Learner Release and Waiver, you or anyone on your behalf make a claim against the Mentor(s) relating to Lesson(s), you will indemnify and hold the Mentor(s) harmless from any liability, demand, loss, damage, or costs which the Mentor(s) may incur as the result of such claim. </li>
                            <li>YOU AFFIRM THAT YOU HAVE READ THIS LEARNER RELEASE AND WAIVER AND FULLY UNDERSTAND THE ASSUMPTION OF RISK, RELEASE, WAIVER, AND CONSENT CONTAINED IN IT. YOU FURTHER UNDERSTAND THAT YOU HAVE GIVEN UP RIGHTS BY AGREEING TO THESE TERMS, AND HAVE DONE SO FREELY AND VOLUNTARILY AND WITHOUT INDUCEMENT. </li>
                        </ul>
                    </ol>
                </div>
                <?php
				do_action( 'storefront_page_after' );

			endwhile; // End of the loop. ?>
        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->
    <?php
get_footer();