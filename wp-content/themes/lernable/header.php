<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */

?>
    <!DOCTYPE html>
    <html <?php language_attributes(); ?>>

    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
        <link rel="profile" href="https://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,600,700">
        <link rel="stylesheet" href="https://lernable.com/wp-content/themes/lernable/font-awesome/css/font-awesome.min.css">
		  <link rel="stylesheet" href="https://lernable.com/wp-content/themes/lernable/style.css">
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <div id="page" class="hfeed site">
            <?php
	do_action( 'storefront_before_header' ); ?>
                <header id="masthead" class="site-header" role="banner" style="<?php storefront_header_styles(); ?>">
                    <?php
			/**
			 * Functions hooked into storefront_header action
			 *
			 * @hooked storefront_skip_links                       - 0
			 * @hooked storefront_social_icons                     - 10
			 * @hooked storefront_site_branding                    - 20
			 * @hooked storefront_secondary_navigation             - 30
			 * @hooked storefront_product_search                   - 40
			 * @hooked storefront_primary_navigation_wrapper       - 42
			 * @hooked storefront_primary_navigation               - 50
			 * @hooked storefront_header_cart                      - 60
			 * @hooked storefront_primary_navigation_wrapper_close - 68
			 */
			//do_action( 'storefront_header' ); ?>
                        <div class="hamburger-buttons">
                            <button class="hamburger">&#9776;</button>
                            <button class="hamburger-cross">&times;</button>
                        </div>
                        <div class="nav-menu left">
                            <?php /* ?>
                            <div class="nav-menu-item">
                                <a href="https://lernable.com/my-account/">
                                    <?php echo is_user_logged_in() ? 'My Account' : 'Login' ?>
                                </a>
                            </div>
                            <?php if(!is_user_logged_in()): ?>
                            <div class="nav-menu-item">
                                <a href="https://lernable.com/sign-up/">Sign Up</a>
                            </div>
                            <?php //endif; ?>
                            <?php */ ?>
                            <div class="nav-menu-item">
                                <a href="https://lernable.com/become-a-mentor/">Become a Mentor</a>
                            </div>
                        </div>
                        <div class="middle">
                            <a href="https://lernable.com/" class="logo-link" rel="home" itemprop="url"><img src="https://lernable.com/wp-content/themes/lernable/images/logo_white.png" class="custom-logo" alt="Lernable" itemprop="logo" /></a>
                        </div>
                        <div class="nav-menu right">
                            <div class="nav-menu-item">
                                <a href="https://lernable.com/how-it-works/">How It Works</a>
                            </div>
                        </div>
                </header>
                <div class="hamburger-menu">
                    <ul>
                        <div class="link">
                            <a href="https://lernable.com/how-it-works/">
                                <li>How it Works</li>
                            </a>
                        </div>
                        <div class="link">
                            <a href="https://lernable.com/become-a-mentor/">
                                <li>Become a Mentor</li>
                            </a>
                        </div>
                        <div class="link">
                            <a href="#">
                                <li class="soon">Login - coming soon</li>
                            </a>
                        </div>
                        <div class="link">
                            <a href="https://lernable.com/terms">
                                <li>Terms</li>
                            </a>
                        </div>
                        <div class="link">
                            <a href="https://lernable.com/privacy">
                                <li>Privacy Policy</li>
                            </a>
                        </div>
                    </ul>
                </div>
                <?php
	/**
	 * Functions hooked in to storefront_before_content
	 *
	 * @hooked storefront_header_widget_region - 10
	 */
	do_action( 'storefront_before_content' ); ?>

                    <div id="content" class="site-content" tabindex="-1">
                        <div class="col-full">

                            <?php
		/**
		 * Functions hooked in to storefront_content_top
		 *
		 * @hooked woocommerce_breadcrumb - 10
		 */
		do_action( 'storefront_content_top' );