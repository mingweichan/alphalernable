<?php
/**
 * Order Item Details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details-item.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
	return;
}
?>
	<div class="order-review">

		<div class="product-name">
			<?php
			$is_visible        = $product && $product->is_visible();
			$product_permalink = apply_filters( 'woocommerce_order_item_permalink', $is_visible ? $product->get_permalink( $item ) : '', $item, $order );

			echo apply_filters( 'woocommerce_order_item_name', $product_permalink ? sprintf( '<a href="%s">%s</a>', $product_permalink, $item->get_name() ) : $item->get_name(), $item, $is_visible );
//			echo apply_filters( 'woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf( '&times; %s', $item->get_quantity() ) . '</strong>', $item );

			do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order );
			
			$product_attr = get_post_meta($item->get_product_id(), '_product_attributes');
				?>
		</div>
		<div class="with-host">
			<?php
                $host_name = $product_attr[0]["name"]["value"];
                echo '- with ' . $host_name;
            ?>
		</div>
		<div class="date">

			<?php
			foreach ( $item->get_formatted_meta_data() as $meta_id => $meta ) {
				if($meta->{"key"} == "day") $day = $meta->{"value"};
				else if($meta->{"key"} == "month") $month = $meta->{"value"};
				else if($meta->{"key"} == "year") $year = $meta->{"value"};
				else if($meta->{"key"} == "time") $time = $meta->{"value"};
				else if($meta->{"key"} == "meridiem") $meridiem = $meta->{"value"};
			}
				//wc_display_item_meta( $item );
				// wc_display_item_downloads( $item );
			
				 $day_names = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
				 $month_names = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
				 $ordinal_endings = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
				 echo $day_names[ intval(date('N', strtotime($year.'-'.$month.'-'.$day))) - 1] . ', ';

				 $day_num = intval($day)%100;
				 if($day_num == '11' || $day_num == '12' || $day_num == '13') {
					  $ordinal_ending = 'th';
				 } else {
					  $ordinal_ending = $ordinal_endings[intval($day)%10];
				 }
				 echo intval($day) . $ordinal_ending . ' ';
				 echo $month_names[intval($month)-1];
            ?>
		</div>
		<div class="time">
			<?php
				$duration_string = $product_attr[0]["duration"]["value"];
            $duration = substr($duration_string, 0, -1);
            $min_or_hr = substr($duration_string, -1);
            if( $min_or_hr == "m" ) {
                $duration_hour = $duration / 60;
                $duration_minute = ':'. sprintf("%02d", $duration % 60);
            } else {
                $duration_hour = $duration;
                $duration_minute = ':00';
            }
          
				$end_time = intval($time) + intval($duration_hour);
				$end_time %= 24;
            $end_meridiem = ' ' . $meridiem;
            if($end_time > 24) $end_time %= 24;
				if($end_time > 12) {
					$end_time %= 12;
					if($meridiem == 'am') {
						$end_meridiem = ' pm';
					} else {
						$end_meridiem = ' am';
					}
				}
				echo $time . ':00 ';
				if($end_meridiem != (' ' . $meridiem)) {
				    echo $meridiem;
				}
				echo ' - ' . $end_time . $duration_minute . $end_meridiem;
          
			?>
		</div>

		<?php do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order ); ?>

		<div class="woocommerce-table__product-total product-total">
			<div class="product">
                <div class="ming-name">
				<?php echo $item->get_name() ; ?>
                </div>
                <p class="ming-name">
                x 1 person
                </p>
			</div>
			<div class="total">
				<?php echo $order->get_formatted_line_subtotal( $item ); ?>
			</div>
		</div>

	</div>

	<hr>

	<?php if ( $show_purchase_note && $purchase_note ) : ?>

	<div class="woocommerce-table__product-purchase-note product-purchase-note">

		<div>
			<?php //echo wpautop( do_shortcode( wp_kses_post( $purchase_note ) ) ); ?>
		</div>

	</div>

	<?php endif; ?>