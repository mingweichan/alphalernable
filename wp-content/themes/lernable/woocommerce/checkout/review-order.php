<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
    <?php
			do_action( 'woocommerce_review_order_before_cart_contents' );

			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                $product_attr = get_post_meta($_product->get_parent_id(), '_product_attributes');
					?>
        <div class="product-name">
            <?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ); ?>
        </div>
        <div class="with-host">
            <?php
                $host_name = $product_attr[0]["name"]["value"];
                echo '- with ' . $host_name;
            ?>
        </div>
        <div class="duration">
            <?php
                $duration_string = $product_attr[0]["duration"]["value"];
                $duration = substr($duration_string, 0, -1);
                $min_or_hr = substr($duration_string, -1);
                if($min_or_hr == "m") {
                    echo $duration . ' minute lesson';
                } else {
                    echo $duration . ' hour lesson';
                }
            ?>
        </div>
        <div class="total-price">
            <?php wc_cart_totals_order_total_html(); ?>
        </div>
        <div class="date">
            <?php
                $v_attributes = $_product->get_variation_attributes();
                $day_names = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
                $month_names = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
                $ordinal_endings = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
                echo $day_names[ intval(date('N', strtotime($v_attributes["attribute_year"].'-'.$v_attributes["attribute_month"].'-'.$v_attributes["attribute_day"]))) - 1] . ', ';
                
                $day_num = intval($v_attributes["attribute_day"])%100;
                if($day_num == '11' || $day_num == '12' || $day_num == '13') {
                    $ordinal_ending = 'th';
                } else {
                    $ordinal_ending = $ordinal_endings[$v_attributes["attribute_day"]%10];
                }
                echo intval($v_attributes["attribute_day"]) . $ordinal_ending . ' ';
                echo $month_names[$v_attributes["attribute_month"]-1];
            ?>
        </div>
        <div class="time">
            <?php
                /* already done
                $duration_string = $product_attr[0]["duration"]["value"];
                $duration = substr($duration_string, 0, -1);
                $min_or_hr = substr($duration_string, -1);*/
                if( $min_or_hr == "m" ) {
                $duration_hour = $duration / 60;
                $duration_minute = ':'. sprintf("%02d", $duration % 60);
                } else {
                    $duration_hour = $duration;
                    $duration_minute = ':00';
                }
                
                $end_time = intval($v_attributes['attribute_time']) + intval($duration_hour);
                $end_time %= 24;
                $meridiem = $v_attributes["attribute_meridiem"];
                $end_meridiem = ' ' . $meridiem;
                if($end_time > 12) {
                    $end_time %= 12;
                    if($meridiem == 'am') {
                        $end_meridiem = ' pm';
                    } else {
                        $end_meridiem = ' am';
                    }
                }
					 echo $v_attributes["attribute_time"] . ':00 ';
					 if((' ' . $meridiem) != $end_meridiem) {
						 echo $meridiem;
					 }
                echo ' - ' . $end_time . $duration_minute . $end_meridiem; 
            ?>
        </div>
        <div class="location">
            <?php echo $product_attr[0]["location"]["value"]; ?>
        </div>
        <div class="languages">
            <?php echo 'Taught in '. $product_attr[0]["languages"]["value"]; ?>
        </div>
        <?php
				}
			}

			do_action( 'woocommerce_review_order_after_cart_contents' );
		?>