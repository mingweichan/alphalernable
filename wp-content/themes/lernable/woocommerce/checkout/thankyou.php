<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

	<div class="woocommerce-order">

		<?php if ( $order ) : ?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>

		<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed">
			<?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?>
		</p>

		<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
			<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay">
				<?php _e( 'Pay', 'woocommerce' ) ?>
			</a>
			<?php if ( is_user_logged_in() ) : ?>
			<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay">
				<?php _e( 'My account', 'woocommerce' ); ?>
			</a>
			<?php endif; ?>
		</p>

		<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>
		<?php else : ?>

		<h1 class="page-title">Your receipt</h1>

		<div class="checkout-top">
			<?php //echo var_dump($order);
				foreach( $order->get_items() as $item_key => $item_values ) {
					$product_id = $item_values->get_product_id();
				};
				
			$product_attr = get_post_meta($product_id, '_product_attributes');
			$checkout_image = $product_attr[0]["checkout_image"]["value"]; ?>
			<div class="checkout-image-container">
				<img src="<?php echo $checkout_image; ?>" class="checkout-image" />
			</div>

			<div id="order_review" class="woocommerce-checkout-review-order">
				<div class="order-review-container">
				<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>
				</div>
			</div>

			
			<?php // setup for column right 
				$name = $product_attr[0]["name"]["value"];
				$address = $product_attr[0]["address"]["value"];
				foreach ( $order->get_items() as $item_id => $item ) {
					foreach ( $item->get_formatted_meta_data() as $meta_id => $meta ) {
						if($meta->{"key"} == "time") $time = $meta->{"value"};
					}
				}
				$details = $product_attr[0]["receipt_details"]["value"];
			?>
			<div class="column-right">
				<p class="thanks">Thanks for booking a lesson with <?php echo $name; ?>!</p>
				<p class="time-and-location">Your lesson is at <strong><?php echo $address; ?></strong> at <strong><?php echo $time . ':00'; ?>.</strong></p>
				<p class="details"><?php echo $details; ?></p>
				<p class="email-reminder">- We've sent you an email with a receipt so that you have all the details on hand.</p>
			</div>
		</div>
		<?php /* ?>
		<ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">

			<li class="woocommerce-order-overview__order order">
				<?php _e( 'Order number:', 'woocommerce' ); ?>
				<strong><?php echo $order->get_order_number(); ?></strong>
			</li>

			<li class="woocommerce-order-overview__date date">
				<?php _e( 'Date:', 'woocommerce' ); ?>
				<strong><?php echo wc_format_datetime( $order->get_date_created() ); ?></strong>
			</li>

			<li class="woocommerce-order-overview__total total">
				<?php _e( 'Total:', 'woocommerce' ); ?>
				<strong><?php echo $order->get_formatted_order_total(); ?></strong>
			</li>

			<?php if ( $order->get_payment_method_title() ) : ?>

			<li class="woocommerce-order-overview__payment-method method">
				<?php _e( 'Payment method:', 'woocommerce' ); ?>
				<strong><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></strong>
			</li>

			<?php endif; ?>

		</ul>

		<?php */ ?>

		<?php endif; ?>

		<?php //do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>

		<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received">
			<?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), null ); ?>
		</p>

		<?php endif; ?>

	</div>