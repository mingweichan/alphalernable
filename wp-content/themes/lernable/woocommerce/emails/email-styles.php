<?php
/**
 * Email Styles
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-styles.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates/Emails
 * @version 2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Load colors
$bg              = get_option( 'woocommerce_email_background_color' );
$body            = get_option( 'woocommerce_email_body_background_color' );
$base            = get_option( 'woocommerce_email_base_color' );
$base_text       = wc_light_or_dark( $base, '#202020', '#ffffff' );
$text            = get_option( 'woocommerce_email_text_color' );

$bg_darker_10    = wc_hex_darker( $bg, 10 );
$body_darker_10  = wc_hex_darker( $body, 10 );
$base_lighter_20 = wc_hex_lighter( $base, 20 );
$base_lighter_40 = wc_hex_lighter( $base, 40 );
$text_lighter_20 = wc_hex_lighter( $text, 20 );

// !important; is a gmail hack to prevent styles being stripped if it doesn't like something.
?>
#wrapper {
	background-color: <?php echo esc_attr( $bg ); ?>;
	margin: 0 10%;
	-webkit-text-size-adjust: none !important;
	width: 600px;
	padding: 30px 0;
}

#template_container {
	box-shadow: 0 1px 4px rgba(0,0,0,0.1) !important;
	background-color: <?php echo esc_attr( $body ); ?>;
	border: 1px solid <?php echo esc_attr( $bg_darker_10 ); ?>;
	border-radius: 3px !important;
}

#template_header {
	background-color: <?php echo esc_attr( $base ); ?>;
	border-radius: 3px 3px 0 0 !important;
	color: <?php echo esc_attr( $base_text ); ?>;
	border-bottom: 0;
	font-weight: bold;
	line-height: 100%;
	vertical-align: middle;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
}

#template_footer td {
	padding: 0;
	-webkit-border-radius: 6px;
}

#template_footer #credit {
	border:0;
	color: <?php echo esc_attr( $base_lighter_40 ); ?>;
	font-family: Arial;
	font-size:12px;
	line-height:125%;
	text-align:center;
	padding: 0 48px 48px 48px;
}

#body_content {
	background-color: <?php echo esc_attr( $body ); ?>;
}

#body_content table td {
	padding: 48px;
}

#body_content table td td {
	padding: 12px;
}

#body_content table td th {
	padding: 12px;
}

#body_content td ul.wc-item-meta {
	font-size: small;
	margin: 1em 0 0;
	padding: 0;
	list-style: none;
}

#body_content td ul.wc-item-meta li {
	margin: 0.5em 0 0;
	padding: 0;
}

#body_content td ul.wc-item-meta li p {
	margin: 0;
}

#body_content p {
	margin: 0 0 16px;
}

#body_content_inner {
	color: <?php echo esc_attr( $text_lighter_20 ); ?>;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 14px;
	line-height: 150%;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

.td {
	color: <?php echo esc_attr( $text_lighter_20 ); ?>;
	border: 1px solid <?php echo esc_attr( $body_darker_10 ); ?>;
}

.text {
	color: <?php echo esc_attr( $text ); ?>;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
}

.link {
	color: <?php echo esc_attr( $base ); ?>;
}

#header_wrapper {
	padding: 36px 48px;
	display: block;
}

h1 {
	color: <?php echo esc_attr( $base ); ?>;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 30px;
	font-weight: 300;
	line-height: 150%;
	margin: 0;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
	text-shadow: 0 1px 0 <?php echo esc_attr( $base_lighter_20 ); ?>;
	-webkit-font-smoothing: antialiased;
}

h2 {
	color: <?php echo esc_attr( $base ); ?>;
	display: block;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 18px;
	font-weight: bold;
	line-height: 130%;
	margin: 16px 0 8px;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

h3 {
	color: <?php echo esc_attr( $base ); ?>;
	display: block;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 16px;
	font-weight: bold;
	line-height: 130%;
	margin: 16px 0 8px;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

a {
	color: <?php echo esc_attr( $base ); ?>;
	font-weight: normal;
	text-decoration: underline;
}

img {
	border: none;
	display: inline;
	font-size: 14px;
	font-weight: bold;
	height: auto;
	line-height: 100%;
	outline: none;
	text-decoration: none;
	text-transform: capitalize;
}

// added in styling

#template_header_image img {
	height: 46px;
	width: 100%;
}

h1.email-title {
	font-family: Tahoma;
	font-weight: 300;
	font-size: 24px;
	color: black;
}

h2.says-hi {
	font-family: Tahoma;
	font-weight: 300;
	font-size: 18px;
	color: black;
}

h3.invoice-section-title {
	font-family: Tahoma;
	font-weight: 300;
	font-size: 24px;
	color: black;
	padding-bottom: 10px;
}

p.invoice-section {
	font-family: Tahoma;
	font-weight: 300;
	font-size: 18px;
	color: black;
}

a.contact-us-button {
	font-family: Tahoma;
	font-weight: 300;
	font-size: 18px;
	color: white;
	text-decoration: none;
}

img.invoice-image {
	width: 100%;
}

p.speech-bubble {
	font-family: Tahoma;
	font-weight: 300;
	color: white;
	font-size: 18px;
	margin: 0 5%;
	width: 90%;
	letter-spacing: 0.2px;
	line-height: 26px;
}

div.speech-bubble-container {
	background-color: #5DC6BB;
	width: 100%;
	margin-top: 25px;
	padding: 20px 0;
	border-radius: 5px;
}

p.contact {
	font-family: Tahoma;
	font-weight: 300;
	color: black;
	font-size: 18px;
}

a.host-phone, a.host-email {
	color: #5DC6BB;
	text-decoration: none;
}

hr.invoice-break {
	margin-top: 32px;
	margin-bottom: 25px;
	background-color: black;
	height: 0.9px;
	border: 0;
}

div.contact-us-button {
	width: 126px;
	background-color: #5DC6BB;
	text-align: center;
	padding-top: 10px;
	height: 33px;
	border-radius: 50px;
	margin-top: 20px;
}

a.contact-us-button {
	font-family: Tahoma;
	color: white;
	font-size: 18px;
	text-decoration: none;
}

p.footer-text {
	font-family: Tahoma;
	font-size: 12px;
	color: black;
}

p.lots-of-love {
	font-family: Tahoma;
	font-size: 12px;
	color: black;
}

p.footer-address {
	font-family: Tahoma;
	font-size: 12px;
	color: black;
}

div.invoice-directions {
	width: 150px;
	background-color: #5DC6BB;
	text-align: center;
	padding-top: 10px;
	height: 33px;
	border-radius: 50px;
	margin-top: 20px;
}

a.invoice-directions {
	font-family: Tahoma;
	color: white;
	text-decoration: none;
	font-size: 18px;
}

span.sub-total {
	float: right;
}

img.fa-heart {
	display: inline-block;
}

p.payment-method-date {
	font-family: Tahoma;
	font-size: 12px;
	color: black;
	font-weight: 300;	
}

div.speech-bubble-tail {
  content: ' ';
  position: absolute;
  width: 0;
  height: 0;
  margin-left: 30px;
  border: 32px solid;
  border-color: #5dc6bb transparent transparent;
}

a.review-button {
	font-family: Tahoma;
	color: white;
	text-decoration: none;
	font-size: 18px;
}

div.review-button-container {
	width: 155px;
	background-color: #5DC6BB;
	text-align: center;
	padding-top: 10px;
	height: 33px;
	border-radius: 50px;
	margin-top: 20px;
	margin-bottom: 35px;
}

div.left-padding {
	display: inline-block;
	width: 35%;
}

div.left-icon, div.middle-icon, div.right-icon {
	display: inline-block;
	width: 10%;
}

div.right-icon img {
	float: right;
}

div.middle-icon img {
	padding-left: 27%;
}

img.completed-image {
	width: 100%;
	padding-top: 12px;
}

<?php
