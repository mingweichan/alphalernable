<?php
/**
 * Customer processing order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-processing-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); 
$items = $order->get_items();
foreach ( $items as $item_id => $item ) {
	$lesson_name = $item->get_name();
	$product_variation = $item->get_product();
	//echo var_dump($product);
	foreach ( $item->get_formatted_meta_data() as $meta_id => $meta ) {
		if($meta->{"key"} == "day") $day = $meta->{"value"};
		else if($meta->{"key"} == "month") $month = $meta->{"value"};
		else if($meta->{"key"} == "year") $year = $meta->{"value"};
		else if($meta->{"key"} == "time") $time = $meta->{"value"};
		else if($meta->{"key"} == "meridiem") $meridiem = $meta->{"value"};
	}
}

$product_attr = get_post_meta($product_variation->get_parent_id(), '_product_attributes');

$host_name = $product_attr[0]["name"]["value"];
$invoice_image = $product_attr[0]["invoice_image"]["value"];
$host_message = $product_attr[0]["host_message"]["value"];
$address = $product_attr[0]["address"]["value"];
$state_pcode_country = $product_attr[0]["state_pcode_country"]["value"];
$directions = $product_attr[0]["directions"]["value"];
$host_phone = $product_attr[0]["host_phone"]["value"];
$host_email = $product_attr[0]["host_email"]["value"];

$duration_string = $product_attr[0]["duration"]["value"];
$duration = substr($duration_string, 0, -1);
$min_or_hr = substr($duration_string, -1);
if( $min_or_hr == "m" ) {
	 $duration_hour = $duration / 60;
	 $duration_minute = ':'. sprintf("%02d", $duration % 60);
} else {
	 $duration_hour = $duration;
	 $duration_minute = ':00';
}

$end_time = intval($time) + intval($duration_hour);
$end_meridiem = ' ' . $meridiem;
if($end_time > 24) $end_time %= 24;
if($end_time > 12) {
	 $end_time %= 12;
	 if($meridiem == 'am') {
		  $end_meridiem = ' pm';
	 } else {
		  $end_meridiem = ' am';
	 }
}

$attribute_time = $time .':00 ';
if($end_meridiem != (' ' . $meridiem) ) {
	 $attribute_time .= $meridiem;
}
$attribute_time .= ' - '. $end_time . $duration_minute . $end_meridiem;

if( $min_or_hr == "m" ) {
	$min_or_hr = "minute";
} else {
	$min_or_hr = "hour";
}

$day_names = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
$month_names = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
$ordinal_endings = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
$day_name = $day_names[intval(date('N', strtotime($year.'-'.$month.'-'.$day))) - 1];

$day_num = intval($day)%100;
if($day_num == '11' || $day_num == '12' || $day_num == '13') {
  $ordinal_ending = 'th';
} else {
  $ordinal_ending = $ordinal_endings[intval($day)%10];
}

//price logic
$total = $order->get_formatted_line_subtotal( $item );
$service_fee = round($total * 0.89, 2);
$init_amount = $total - $service_fee;


if($order->get_payment_method()=="braintree_credit_card") {
	$payment_method = "credit card";
} else if ($order->get_payment_method()=="braintree_paypal") {
	$payment_method = "Paypal";
} else {
	$payment_method = $order->get_payment_method();
}

?>

<h1 class="email-title" style="padding-top: 25px;">Booking confirmed for <?php echo $lesson_name; ?></h1>

<h2 class="says-hi" style="padding-bottom: 25px;"><?php echo $host_name; ?> says hi!</h2>

<img src="<?php echo $invoice_image; ?>" class="invoice-image">

<div class="speech-bubble-container"><p class="speech-bubble">"<?php echo $host_message; ?>"</p></div>
<div class="speech-bubble-tail"></div>

<p class="contact" style="margin-top:-10px;">Contact <span class="host-name"><?php echo $host_name; ?></span> at <a class="host-phone" href="tel:<?php echo $host_phone?>"><?php echo $host_phone; ?></a> or at <a class="host-email" href="mailto:<?php echo $host_email; ?>"><?php echo $host_email; ?></a></p>

<hr class="invoice-break">

<h3 class="invoice-section-title">Lesson</h3>
<p class="invoice-section"><?php echo $lesson_name; ?></p>
<hr class="invoice-break">

<h3 class="invoice-section-title">Duration and Time</h3>
<p class="invoice-section"><?php echo $duration . ' ' . $min_or_hr; ?> lesson at <?php echo $attribute_time . ', ' . $day_name . ' ' . intval($day) . $ordinal_ending . ' ' . $month_names[intval($month)-1]; ?></p>
<hr class="invoice-break">

<h3 class="invoice-section-title">Location</h3>
<p class="invoice-section"><?php echo $address . ', ' . $state_pcode_country ?></p>
<div class="invoice-directions"><a href="<?php echo $directions; ?>" class="invoice-directions">Get Directions</a></div>
<hr class="invoice-break">

<h3 class="invoice-section-title">Amount</h3>
<p class="invoice-section"><?php echo $total; ?> AUD x 1 lesson <span class="sub-total"><?php echo $total; ?> AUD</span></p>
<!-- <p class="invoice-section">Lernable Service Fee (+GST)<span class="sub-total"></span></p> -->
<hr class="invoice-break">

<h3 class="invoice-section-title">Payment Method</h3>
<p class="invoice-section">Paid with <?php echo $payment_method; ?><span class="sub-total"><?php echo $total; ?> AUD</span></p>
<p class="payment-method-date" style="margin-top:-13px;"><?php echo $order->get_date_paid()->date("l, jS F, Y"); ?></p>
<hr class="invoice-break">

<h3 class="invoice-section-title">Other Details</h3>
<p class="invoice-section">Booking number: <?php echo $order->get_order_number(); ?></p>
<p class="invoice-section">Booked by <?php echo $order->get_billing_first_name() . ' ' . $order->get_billing_last_name(); ?> on the <?php echo $order->get_date_paid()->date("jS F, Y"); ?></p>
<p class="invoice-section">Payments processed by Lernable Co. ABN 54 510 149 855</p>
<hr class="invoice-break">

<h3 class="invoice-section-title">Customer Support</h3>
<div class="contact-us-button"><a href="mailto:contact@lernable.com" class="contact-us-button">Contact Us</a></div>
<hr class="invoice-break">

<?

// send email to host with booking details

$host_email_message =
	'<!DOCTYPE html>' .
	'<html>' .
		'<head>' .
			'<title>New Booking</title>' .
			'<link rel="stylesheet" href="https://lernable.com/wp-content/themes/lernable/style.css">' .
		'</head>' .
		'<body>' .
			'<div class="new-booking-container">' .
				'<p class="booking-text">Lesson: ' . $lesson_name . ' (ID: ' . $order->get_order_number() . ')</p>' .
				'<p class="booking-text">Booking Time: ' . $attribute_time . ', ' . $day_name . ' ' . intval($day) . $ordinal_ending . ' ' . $month_names[intval($month)-1] . '</p>' .
				'<p class="booking-text">Booking Number: ' . $order->get_order_number() . '</p>' .
				'<p class="booking-text">Customer: ' . $order->get_billing_first_name() . ' ' . $order->get_billing_last_name() . ' (Email: ' . $order->get_billing_email() . ')</p>' .
			'</div>' .
		'</body>' .
	'</html>';

//wp_mail( $host_email, 'Lernable - New Booking for ' . $attribute_time . ', ' . $day_name . ' ' . intval($day) . $ordinal_ending . ' ' . $month_names[intval($month)-1] , $host_email_message, 'From: Lernable <contact@lernable.com>');

// send email to contact@lernable.com with booking details

wp_mail( 'contact@lernable.com' , 'New Booking (ID: ' . $order->get_order_number() . ')' , $host_email_message );

/*
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
//do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
//do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/**
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */

/**
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
