<?php
/**
 * Customer completed order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-completed-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email );

$items = $order->get_items();
foreach ( $items as $item_id => $item ) {
	$lesson_name = $item->get_name();
	$product_id = $item->get_product()->get_parent_id();
	$product_attr = get_post_meta($product_id, '_product_attributes');
}


$host_name = $product_attr[0]["name"]["value"];
$completed_image = $product_attr[0]["completed_image"]["value"];

$sex = $product_attr[0]["sex"]["value"];
if($sex == "him") {
	$he_she = "he";
} else {
	$he_she = "she";
}

?>


<h1 class="email-title">What did you think about <?php echo $lesson_name; ?>?</h1>

<p class="invoice-section">Write a review to tell <?php echo $host_name; ?> what you loved and what <?php echo $he_she; ?> could have done a bit better. Your feedback is incredibly important to us, as it helps us refine the experience for others and hopefully also for your future lessons!</p>

<h1 class="email-title">Review and receive a free coffee on us.*</h1>

<div class="review-button-container"><a class="review-button" href="https://www.lernable.com/add-review/?id=<?php echo $product_id; ?>">Write a review</a></div>

<p class="invoice-section">Or tell us on social media:</p>

<div class="social-media-icons">
	<div class="left-padding"></div>
	<div class="left-icon"><a href="https://www.facebook.com/Lernable/"><img class="social-media-icons" src="https://lernable.com/wp-content/themes/lernable/images/fa_facebook.png"></a></div>
	<div class="middle-icon"><a href="https://twitter.com/lernable/"><img class="social-media-icons" src="https://lernable.com/wp-content/themes/lernable/images/fa_twitter.png"></a></div>
	<div class="right-icon"><a href="https://www.instagram.com/lernable/"><img class="social-media-icons" src="https://lernable.com/wp-content/themes/lernable/images/fa_instagram.png"></a></div>
</div>

<p class="invoice-section">(we'll still buy you a coffee)</p>

<img class="completed-image" src="<?php echo $completed_image; ?>">

<p class="invoice-section">*only available for those able to meet us in Sydney!</p>
<hr class="invoice-break">

<?php
/**
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
//do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/**
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
//do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
