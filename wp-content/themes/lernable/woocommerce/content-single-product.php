<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
    <?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

if(isset($_POST['mobile-calendar'])): ?>
        <div class="mobile-calendar">
            <?php lernable_book(); ?>
        </div>
        <?php else: ?>
            <?php
	if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }

    global $product;
    $name = $product->get_attribute( 'name' );
    $sex = $product->get_attribute( 'sex' );
//$foo = 'hero_image';
//$bar = $product->get_attribute($foo);
//print($bar);
//
//$n = 1;
//if($bar = ""){
//    return;
//}else{
//    for ($n = 1;$n < ; $n++){
//    
//    $bar = $foo . $n;
//    print($bar);
//    $bar = $product->get_attribute($foo . $n);
//    }
//}
//    $hero_image = $product->get_attribute($foo );
//	 $hero_image2 = $product->get_attribute($foo . 2);
//	 $hero_image3 = $product->get_attribute( 'hero_image3' );
    $map = $product->get_attribute('map');
    $tagline = $product->get_attribute('tagline');
    $size_of_lesson = $product->get_attribute('size_of_lesson');
?>
                <div id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <div class="listing-hero">
                        <?php
                        $n = $product->get_attribute('number_of_images');
                   /*
                        for ($x = 1; $x <= $n; $x++){
                            $img = $product->get_attribute('hero_image' . $x);
                           echo '<span class="carousel-badge" onclick="currentDiv(' . $x . ')"></span>';
                        }
                    */
                        $x = 1;
                        while(($img = $product->get_attribute('hero_image' . $x)) != null) {
                            echo '<img src="' . $img . '" class="hero-image"/>';
                            $x += 1;
                        };
                    ?>
                            <!--
					<img src="<?php
//echo $hero_image;
?>" class="hero-image" />
					<img src="<?php 
//echo $hero_image2;
?>" class="hero-image" />
					<img src="<?php
//echo $hero_image3;
?>" class="hero-image" />
-->
                            <div class="hero-controls">
                                <?php 
                                
                                if($n > 1){
                                    echo '<div class="left-arrow" onclick="prevDiv()"><img src="https://lernable.com/wp-content/uploads/2017/10/seemorearrownewleft.png" alt="left arrow"></div>
                                <div class="right-arrow" onclick="nextDiv()"><img src="https://lernable.com/wp-content/uploads/2017/10/seemorearrownew.png" alt="right arrow"></div>';
                                };
                                    ?> </div>
                            <h1 class="tagline" id="tagline">Meet
						<?php echo $name ?>.</h1>
                            <h2 class="description" id="description">
						<?php echo $tagline ?>
					</h2>
                            <div class="badges">
                                <?php
                        
                   
                      
                                    $x = 1;
                                    while(($img = $product->get_attribute('hero_image' . $x)) != null) {
                                        if($x > 1){
                                            if($x == 2){
                                                echo '<span class="carousel-badge" onclick="currentDiv(1)"></span>';
                                            } 
                                        
                                            echo '<span class="carousel-badge" onclick="currentDiv(' . $x . ')"></span>';
                                        }
                                        $x += 1;
                                        
                                    };
                    ?>
                                    <!--
						<span class="carousel-badge" onclick="currentDiv(1)"></span>
						<span class="carousel-badge" onclick="currentDiv(2)"></span>
						<span class="carousel-badge" onclick="currentDiv(3)"></span>
--></div>
                    </div>
                    <div class="summary entry-summary">
                        <?php
    		/**
		 * woocommerce_before_single_product_summary hook.
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
       * @hooked lernable_book - 30
		 */
		//do_action( 'woocommerce_before_single_product_summary' );
                ?>
                            <div class="info-block-container">
                                <div class="info-block">
                                    <div class="info-block-top">
                                        <div class="single-left-wrapper">
                                            <?php
			/**
			 * woocommerce_single_product_summary hook.
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_price - 6
			 * @hooked woocommerce_template_single_rating - 7
		    * @hooked woocommerce_template_single_excerpt - 8
          * @hooked lernable_contact_text - 9
          * @hooked lernable_location_languages - 10
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 * @hooked WC_Structured_Data::generate_product_data() - 60
			 */
			do_action( 'woocommerce_single_product_summary' );
            
		?>
                                                <!-- The Modal -->
                                                <div id="modal-thanks" class="modal" <?php if(isset($_GET[ 'contacted'])) { echo 'style="display:block;"'; } ?> >
                                                    <!-- Modal content -->
                                                    <div class="modal-content-thanks"> <span id="close-thanks" class="close">&times;</span>
                                                        <div class="modal-container">
                                                            <div class="thanks-title-container">
                                                                <div class="artist-bio-img-thanks"> <img src="<?php echo $product->get_attribute( 'image' ); ?>" alt="" class="alignnone size-medium wp-image-108" /> </div>
                                                                <div class="modal-thanks-text">
                                                                    <h1 class="thanks-title">Thanks for contacting me!</h1>
                                                                    <p class="getback">I'll get back to you real soon.</p>
                                                                </div>
                                                            </div>
                                                            <div class="back-button">
                                                                <button id="back-to-listing">Back to listing</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="modal-form" class="modal">
                                                    <div class="modal-content-form"> <span id="close-form" class="close">&times;</span>
                                                        <div class="modal-container">
                                                            <div class="contact-title-container">
                                                                <div class="artist-bio-img-form"> <img src="<?php echo $product->get_attribute( 'image' ); ?>" alt="" class="alignnone size-medium wp-image-108" /> </div>
                                                                <h1 class="contact-title">Contact
																<?php echo $name; ?>
															</h1> </div>
                                                            <form action="https://www.lernable.com/contact-host-message" method="post">
                                                                <div class="name">
                                                                    <label for="lernable_name">Name: </label>
                                                                    <br>
                                                                    <input type="text" name="lernable_name" class="nameinput"> </div>
                                                                <div class="email">
                                                                    <label for="lernable_email">Email: </label>
                                                                    <br>
                                                                    <input type="text" name="lernable_email" class="emailinput"> </div>
                                                                <div class="message">
                                                                    <label for="lernable_message">Message: </label>
                                                                    <br>
                                                                    <textarea name="lernable_message" class="messageinput"></textarea>
                                                                </div>
                                                                <input type="hidden" name="lernable_hostname" value="<?php echo $name; ?>">
                                                                <input type="hidden" name="lernable_productid" value="<?php echo $product->get_id(); ?>">
                                                                <input type="hidden" name="lernable_permalink" value="<?php echo get_permalink(); ?>">
                                                                <div class="sendbtn">
                                                                    <input type="submit" name="lernable_submit" value="Send"> </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="single-right-wrapper">
                                            <?php lernable_book(); ?>
                                        </div>
                                    </div>
                                    <div class="lern-description">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            </div>
                            <iframe width="100%" height="450" frameborder="0" style="border:0" src="<?php echo $map ?>" allowfullscreen> </iframe>
                            <div class="lern-reviews">
                                <?php comments_template(); ?>
                            </div>
                            <?php
		/**
		 * woocommerce_after_single_product_summary hook.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		//do_action( 'woocommerce_after_single_product_summary' );
	?>
                                <form action="<?php get_permalink(); ?>" method="post">
                                    <input type="hidden" name="mobile-calendar" value="show" />
                                    <input type="submit" class="mobile-calendar-button" id="mobile_calendar_button" value="see availabilities" /> </form>
                    </div>
                    <!-- #product-<?php the_ID(); ?> -->
                    <?php endif; ?>
                        <?php do_action( 'woocommerce_after_single_product' ); ?>