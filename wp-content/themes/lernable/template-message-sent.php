<?php
/**
 * The template for displaying the message sent page.
 *
 * Template Name: Message Sent
 *
 * @package lernable
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post();

				do_action( 'storefront_page_before' );

				get_template_part( 'content', 'page' );

if(isset($_POST['submit'])){
    $to = "contact@lernable.com"; // this is your Email address
    $from = $_POST['email']; // this is the sender's Email address
    $name = $_POST['name'];
    $subject = "Form submission";
    $subject2 = "Copy of your form submission";
    $message = $name . " wrote the following:" . "\n\n" . $_POST['message'];
    $message2 = "Here is a copy of your message " . $name . "\n\n" . $_POST['message'];

    $headers = "From:" . $from;
    $headers2 = "From:" . $to;
    mail($to,$subject,$message,$headers);
    mail($from,$subject2,$message2,$headers2); // sends a copy of the message to the sender
    echo "<h2>Message Sent.</h2>";
    echo "<p>Thank you " . $name . ", we will contact you shortly.</p>";
} else {
    echo "<h2>Message Failed to Send.</h2>";
    echo "<p>Please Try Again.</p>";
}

				/**
				 * Functions hooked in to storefront_page_after action
				 *
				 * @hooked storefront_display_comments - 10
				 */

				do_action( 'storefront_page_after' );

			endwhile; // End of the loop. ?>

		</main>
		<!-- #main -->
	</div>
	<!-- #primary -->

	<?php
get_footer();