<?php
/**
 * The template for displaying the listing request page.
 *
 * Template Name: Listing Request
 *
 * @package lernable
 */

get_header(); ?>

    <?php 
if(isset($_POST['submit'])){
    $to = "contact@lernable.com"; // this is your Email address
    $from = $_POST['email']; // this is the sender's Email address
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $subject = "Form submission";
    $subject2 = "Copy of your form submission";
    $message = $first_name . " " . $last_name . " wrote the following:" . "\n\n" . $_POST['message'];
    $message2 = "Here is a copy of your message " . $first_name . "\n\n" . $_POST['message'];

    $headers = "From:" . $from;
    $headers2 = "From:" . $to;
    mail($to,$subject,$message,$headers);
    mail($from,$subject2,$message2,$headers2); // sends a copy of the message to the sender
    echo "Mail Sent. Thank you " . $first_name . ", we will contact you shortly.";
        // Should redirect to another page to avoid resubmission
    // You can also use header('Location: thank_you.php'); to redirect to another page.
    }
?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

            <h2>Do you want to learn something we don't have yet?</h2>
            <p>(Emails contact@lernable.com)</p>
            
            <form action="" method="post">
                First Name: <input type="text" name="first_name"><br>
                Last Name: <input type="text" name="last_name"><br>
                Email: <input type="text" name="email"><br> 
                Short description of desired listing:
                <br><textarea rows="5" name="message" cols="30"></textarea><br>
                <input type="submit" name="submit" value="Submit">
            </form>

        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->

    <?php
get_footer();