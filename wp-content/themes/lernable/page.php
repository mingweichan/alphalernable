<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package storefront
 */

get_header(); ?>
    <?php do_action( 'storefront_sidebar' );?>
    <?php WC()->cart->empty_cart() ?>

    <?php if ( post_password_required() ) : ?>
    <?php get_the_password_form(); ?>
    <?php endif; ?>

    <div class="home-hero homebanner">

        <h1 class="tagline">a platform for face-to-face learning experiences</h1>
        <div class="hero-button-container">
            <div class="hero-desc-button">
                <a href="https://lernable.com/how-it-works" class="hero-description">find out more</a>
            </div>
        </div>
    </div>
    <div id="primary" class="content-area">

        <main id="main" class="site-main" role="main">

            <div id="listings">
                <h2>Book a lesson</h2>
                <ul class="products">

                    <?php
		$args = array(
			'post_type' => 'product',
			'posts_per_page' => 16
          
			);
		$loop = new WP_Query( $args );
		if ( $loop->have_posts() ) {
			while ( $loop->have_posts() ) : $loop->the_post();
				wc_get_template_part( 'content', 'product' );
			endwhile;
		} else {
			echo __( 'No products found' );
		}
		
	?>
                </ul>
                <!--
                <h2 class="category">Art</h2>
                <ul class="products">
                    <?php /*
		$args = array(
			'post_type' => 'product',
			'posts_per_page' => 12,
            'product_cat' => 'art'
			);
		$loop = new WP_Query( $args );
		if ( $loop->have_posts() ) {
			while ( $loop->have_posts() ) : $loop->the_post();
				wc_get_template_part( 'content', 'product' );
			endwhile;
		} else {
			echo __( 'No products found' );
		}
		

                 ?>
                <a href="https://lernable.com/product-category/art">See more</a>
                </ul>
                <h2 class="category">Music</h2>
                <ul class="products">
                    <?php
		$args = array(
			'post_type' => 'product',
			'posts_per_page' => 12,
            'product_cat' => 'music'
			);
		$loop = new WP_Query( $args );
		if ( $loop->have_posts() ) {
			while ( $loop->have_posts() ) : $loop->the_post();
				wc_get_template_part( 'content', 'product' );
			endwhile;
		} else {
			echo __( 'No products found' );
		}
		

	?>
                <a href="https://lernable.com/product-category/music">See more</a>
                </ul>


                <?php while ( have_posts() ) : the_post();

				do_action( 'storefront_page_before' );

            
//				get_template_part( 'content', 'page' );

				/**
				 * Functions hooked in to storefront_page_after action
				 *
				 * @hooked storefront_display_comments - 10
				 *
				do_action( 'storefront_page_after' );

			endwhile; // End of the loop. */ ?> -->

                <?php

get_footer(); ?>